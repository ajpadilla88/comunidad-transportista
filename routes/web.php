<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Front\HomeController@index')->name('root');
Auth::routes();
Route::get('/camiones', 'Front\CamionesController@index')->name('front.camiones.index');
Route::get('/camiones/test', 'Front\CamionesController@show')->name('front.camiones.show');
Route::get('/buses', 'Front\BusesController@index')->name('front.buses.index');
Route::get('/buses/test', 'Front\BusesController@show')->name('front.buses.show');
Route::get('/utilitarios', 'Front\UtilitariosController@index')->name('front.utilitarios.index');
Route::get('/utilitarios/test', 'Front\UtilitariosController@show')->name('front.utilitarios.show');
Route::get('/noticias', 'Front\NoticiasController@index')->name('front.noticias.index');
Route::get('/noticias/test', 'Front\NoticiasController@show')->name('front.noticias.show');


Route::get('/contacto', 'Front\ContactController@index')->name('front.contacto.index');
Route::post('/contacto', 'Front\ContactController@store')->name('front.contacto.store');

Route::get('/searchTrucks', 'Front\CamionesController@search')->name('front.search.trucks');
Route::get('/searchBuses', 'Front\BusesController@search')->name('front.search.buses');
Route::get('/searchUtilities', 'Front\UtilitariosController@search')->name('front.search.utilities');

//Route::get('/nos-apoyan', 'Front\HomeController@nosapoyan')->name('front.nosapoyan');
//Route::get('/organizaciones-aliadas', 'Front\HomeController@aliados')->name('front.aliados');
//Route::get('/biblioteca', 'Front\HomeController@biblioteca')->name('front.biblioteca');
//Route::get('/events', 'Front\HomeController@events')->name('front.events');
//Route::get('/construccion', 'Front\HomeController@construction')->name('front.construction');

// Account Panel
Route::group(['prefix' => 'account', 'as' => 'account.', 'namespace' => 'Account', 'middleware' => ['auth', 'auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');

    Route::resource('listings', 'ListingsController');
    Route::resource('notifications', 'NotificationsController');
    Route::resource('messages', 'MessagesController');

    Route::get('vehicles/filter', 'VehicleController@filter');
    Route::resource('vehicles', 'VehicleController');
    Route::resource('file', 'FileController');

    Route::get('profile/show', [
		'as' => 'user.profile.show',
		'uses' => 'UserController@show'
	]);

	Route::patch('profile/update/{id}', [
		'as' => 'user.profile.update',
		'uses' => 'UserController@update'
	]);

});

// Admin Panel
Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth', 'auth.admin']], function () {
    Route::get('/home', 'HomeController@index')->name('home');

    Route::resource('users', 'UsersController');
//    Route::resource('activities', 'ActivitiesController');
//    Route::resource('actions', 'ActionsController');
//    Route::resource('habits', 'HabitsController');
//    Route::resource('notifications', 'NotificationsController');
//    Route::resource('messages', 'MessagesController');
});
