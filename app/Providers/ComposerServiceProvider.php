<?php
namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;

class ComposerServiceProvider extends ServiceProvider {

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        // Using class based composers...
        View::composer('admin.layouts.alerts', 'App\Http\ViewComposers\Admin\AlertsComposer');
        View::composer('admin.layouts.sidebar', 'App\Http\ViewComposers\Admin\SidebarComposer');
        View::composer('admin.layouts.nav', 'App\Http\ViewComposers\Admin\NavComposer');

        // Using class based composers...
        View::composer('account.layouts.alerts', 'App\Http\ViewComposers\Account\AlertsComposer');
        View::composer('account.layouts.sidebar', 'App\Http\ViewComposers\Account\SidebarComposer');
        View::composer('account.layouts.nav', 'App\Http\ViewComposers\Account\NavComposer');
    }

    /**
     * Register
     *
     * @return void
     */
    public function register()
    {
        //
    }
}