<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModeloBus extends Model
{
    public $table = 'modelo_buses';
}
