<?php
namespace App\Repositories;

use App\Potencia;

class PotenciaRepository extends AbstractRepository
{
    function __construct(Potencia $model)
    {
        $this->model = $model;
    }

    public function search(array $params = [])
    {
        $query = $this->model;

        if (isset($params['email'])) {
            $query = $query->ofEmail($params['email']);
        }

        return $query;
    }

    

}