<?php
namespace App\Repositories;

use App\TraccionCamion;

class TraccionCamionRepository extends AbstractRepository
{
    function __construct(TraccionCamion $model)
    {
        $this->model = $model;
    }

    public function search(array $params = [])
    {
        $query = $this->model;

        if (isset($params['email'])) {
            $query = $query->ofEmail($params['email']);
        }

        return $query;
    }

    

}