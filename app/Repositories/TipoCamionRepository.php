<?php
namespace App\Repositories;

use App\TipoCamion;

class TipoCamionRepository extends AbstractRepository
{
    function __construct(TipoCamion $model)
    {
        $this->model = $model;
    }

    public function search(array $params = [])
    {
        $query = $this->model;

        if (isset($params['email'])) {
            $query = $query->ofEmail($params['email']);
        }

        return $query;
    }

    

}