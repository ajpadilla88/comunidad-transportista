<?php
namespace App\Repositories;

use App\TipoUtilitario;

class TipoUtilitarioRepository extends AbstractRepository
{
    function __construct(TipoUtilitario $model)
    {
        $this->model = $model;
    }

    public function search(array $params = [])
    {
        $query = $this->model;

        if (isset($params['email'])) {
            $query = $query->ofEmail($params['email']);
        }

        return $query;
    }

    

}