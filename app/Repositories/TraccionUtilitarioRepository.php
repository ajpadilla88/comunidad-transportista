<?php
namespace App\Repositories;

use App\TraccionUtilitario;

class TraccionUtilitarioRepository extends AbstractRepository
{
    function __construct(TraccionUtilitario $model)
    {
        $this->model = $model;
    }

    public function search(array $params = [])
    {
        $query = $this->model;

        if (isset($params['email'])) {
            $query = $query->ofEmail($params['email']);
        }

        return $query;
    }

    

}