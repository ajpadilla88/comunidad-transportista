<?php
namespace App\Repositories;

use App\EquipamientoCamion;

class EquipamientoCamionRepository extends AbstractRepository
{
    function __construct(EquipamientoCamion $model)
    {
        $this->model = $model;
    }

    public function search(array $params = [])
    {
        $query = $this->model;

        if (isset($params['email'])) {
            $query = $query->ofEmail($params['email']);
        }

        return $query;
    }

    

}