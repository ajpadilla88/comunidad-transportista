<?php
namespace App\Repositories;

use App\Kilometraje;

class KilometrajeRepository extends AbstractRepository
{
    function __construct(Kilometraje $model)
    {
        $this->model = $model;
    }

    public function search(array $params = [])
    {
        $query = $this->model;

        if (isset($params['email'])) {
            $query = $query->ofEmail($params['email']);
        }

        return $query;
    }

    public function getByEmail($email)
    {
        return $this->search(compact('email'))->first();
    }

}