<?php
namespace App\Repositories;

use App\AlturaCabina;

class AlturaCabinaRepository extends AbstractRepository
{
    function __construct(AlturaCabina $model)
    {
        $this->model = $model;
    }

    public function search(array $params = [])
    {
        $query = $this->model;

        if (isset($params['marca_id'])) {
            $query = $query->where('marca_id', $params['marca_id']);
        }

        return $query;
    }

    // public function getByEmail($email)
    // {
    //     return $this->search(compact('email'))->first();
    // }

}