<?php
namespace App\Repositories;

use App\TipoBus;

class TipoBusRepository extends AbstractRepository
{
    function __construct(TipoBus $model)
    {
        $this->model = $model;
    }

    public function search(array $params = [])
    {
        $query = $this->model;

        if (isset($params['email'])) {
            $query = $query->ofEmail($params['email']);
        }

        return $query;
    }

    

}