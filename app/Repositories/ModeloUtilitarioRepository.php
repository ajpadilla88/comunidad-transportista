<?php
namespace App\Repositories;

use App\ModeloUtilitario;

class ModeloUtilitarioRepository extends AbstractRepository
{
    function __construct(ModeloUtilitario $model)
    {
        $this->model = $model;
    }

    public function search(array $params = [])
    {
        $query = $this->model;

        if (isset($params['marca_id'])) {
            $query = $query->where('marca_id', $params['marca_id']);
        }

        return $query;
    }

    public function getByMarca($marca_id)
    {
        return $this->search(compact('marca_id'));
    }

}