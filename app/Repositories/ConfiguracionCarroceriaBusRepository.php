<?php
namespace App\Repositories;

use App\ConfiguracionCarroceriaBus;

class ConfiguracionCarroceriaBusRepository extends AbstractRepository
{
    function __construct(ConfiguracionCarroceriaBus $model)
    {
        $this->model = $model;
    }

    public function search(array $params = [])
    {
        $query = $this->model;

        if (isset($params['email'])) {
            $query = $query->ofEmail($params['email']);
        }

        return $query;
    }

   

}