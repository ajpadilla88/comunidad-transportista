<?php
namespace App\Repositories;

use App\MarcaCarroceriaBus;

class MarcaCarroceriaBusRepository extends AbstractRepository
{
    function __construct(MarcaCarroceriaBus $model)
    {
        $this->model = $model;
    }

    public function search(array $params = [])
    {
        $query = $this->model;

        if (isset($params['email'])) {
            $query = $query->ofEmail($params['email']);
        }

        return $query;
    }

    

}