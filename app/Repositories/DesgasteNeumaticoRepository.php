<?php
namespace App\Repositories;

use App\DesgasteNeumatico;

class DesgasteNeumaticoRepository extends AbstractRepository
{
    function __construct(DesgasteNeumatico $model)
    {
        $this->model = $model;
    }

    public function search(array $params = [])
    {
        $query = $this->model;

        if (isset($params['email'])) {
            $query = $query->ofEmail($params['email']);
        }

        return $query;
    }

    

}