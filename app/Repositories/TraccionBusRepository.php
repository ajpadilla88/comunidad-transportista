<?php
namespace App\Repositories;

use App\TraccionBus;

class TraccionBusRepository extends AbstractRepository
{
    function __construct(TraccionBus $model)
    {
        $this->model = $model;
    }

    public function search(array $params = [])
    {
        $query = $this->model;

        if (isset($params['email'])) {
            $query = $query->ofEmail($params['email']);
        }

        return $query;
    }

    

}