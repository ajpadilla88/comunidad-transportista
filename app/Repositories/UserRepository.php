<?php
namespace App\Repositories;

use App\User;

class UserRepository extends AbstractRepository
{
    function __construct(User $model)
    {
        $this->model = $model;
    }

    public function search(array $params = [])
    {
        $query = $this->model;

        if (isset($params['email'])) {
            $query = $query->ofEmail($params['email']);
        }

        return $query;
    }

    public function getByEmail($email)
    {
        return $this->search(compact('email'))->first();
    }

}