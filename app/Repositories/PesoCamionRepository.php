<?php
namespace App\Repositories;

use App\PesoCamion;

class PesoCamionRepository extends AbstractRepository
{
    function __construct(PesoCamion $model)
    {
        $this->model = $model;
    }

    public function search(array $params = [])
    {
        $query = $this->model;

        if (isset($params['email'])) {
            $query = $query->ofEmail($params['email']);
        }

        return $query;
    }

    

}