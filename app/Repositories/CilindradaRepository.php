<?php
namespace App\Repositories;

use App\Cilindrada;

class CilindradaRepository extends AbstractRepository
{
    function __construct(Cilindrada $model)
    {
        $this->model = $model;
    }

    public function search(array $params = [])
    {
        $query = $this->model;

        if (isset($params['email'])) {
            $query = $query->ofEmail($params['email']);
        }

        return $query;
    }

    

}