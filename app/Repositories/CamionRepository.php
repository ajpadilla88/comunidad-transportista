<?php
namespace App\Repositories;

use App\Camion;

class CamionRepository extends AbstractRepository
{
    const PAGINATE = true;
    public $filters = ['keyWords', 'priceRange','priceRange2','location', 'applicationForUse'];

    function __construct(Camion $model)
    {
        $this->model = $model;
    }   

    public function filterByKeyWords($query, $data = array()){
        $query->where('title', 'LIKE', '%' . $data['keyWords'] . '%');
    }

    public function filterByPriceRange($query, $data = array()){
        $query->whereBetween('price',[(int)$data['priceRange'], (int)$data['priceRange2']]);
    }

    public function filterByLocation($query, $data = array()){
        $query->where('state_id','=',$data['location']);
    }

    public function filterByApplicationForUse($query, $data = array()){
        $query->where('aplicacion_uso_id','=',$data['applicationForUse']);
    }

}