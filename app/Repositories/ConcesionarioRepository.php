<?php
namespace App\Repositories;

use App\Concesionario;

class ConcesionarioRepository extends AbstractRepository
{
    function __construct(Concesionario $model)
    {
        $this->model = $model;
    }

}