<?php
namespace App\Repositories;

use App\Transmision;

class TransmisionRepository extends AbstractRepository
{
    function __construct(Transmision $model)
    {
        $this->model = $model;
    }

    public function search(array $params = [])
    {
        $query = $this->model;

        if (isset($params['email'])) {
            $query = $query->ofEmail($params['email']);
        }

        return $query;
    }

    

}