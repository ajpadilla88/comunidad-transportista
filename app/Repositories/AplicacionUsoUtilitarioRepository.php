<?php
namespace App\Repositories;

use App\AplicacionUsoUtilitario;

class AplicacionUsoUtilitarioRepository extends AbstractRepository
{
    function __construct(AplicacionUsoUtilitario $model)
    {
        $this->model = $model;
    }

    public function search(array $params = [])
    {
        $query = $this->model;

        if (isset($params['marca_id'])) {
            $query = $query->where('marca_id', $params['marca_id']);
        }

        return $query;
    }

    // public function getByEmail($email)
    // {
    //     return $this->search(compact('email'))->first();
    // }

}