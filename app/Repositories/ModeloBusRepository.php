<?php
namespace App\Repositories;

use App\ModeloBus;

class ModeloBusRepository extends AbstractRepository
{
    function __construct(ModeloBus $model)
    {
        $this->model = $model;
    }

    public function search(array $params = [])
    {
        $query = $this->model;

        
        if (isset($params['marca_id'])) {
            $query = $query->where('marca_id','=', $params['marca_id']);
            // dd($query->query);
        }

        return $query;
    }

    public function getByMarca($marca_id)
    {
        return $this->search(compact('marca_id'));
    }

}