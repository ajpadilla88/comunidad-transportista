<?php
namespace App\Repositories;

use App\EquipamientoBus;

class EquipamientoBusRepository extends AbstractRepository
{
    function __construct(EquipamientoBus $model)
    {
        $this->model = $model;
    }

    public function search(array $params = [])
    {
        $query = $this->model;

        if (isset($params['email'])) {
            $query = $query->ofEmail($params['email']);
        }

        return $query;
    }

    

}