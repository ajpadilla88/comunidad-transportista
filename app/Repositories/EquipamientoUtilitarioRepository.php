<?php
namespace App\Repositories;

use App\EquipamientoUtilitario;

class EquipamientoUtilitarioRepository extends AbstractRepository
{
    function __construct(EquipamientoUtilitario $model)
    {
        $this->model = $model;
    }

    public function search(array $params = [])
    {
        $query = $this->model;

        if (isset($params['email'])) {
            $query = $query->ofEmail($params['email']);
        }

        return $query;
    }

    

}