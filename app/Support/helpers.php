<?php

if (!function_exists('current_user')) {
    function current_user()
    {
        return Auth::user();
    }
}

if (!function_exists('current_user_id')) {
    function current_user_id()
    {
        if ($user = current_user()) {
            return $user->id;
        }

        return null;
    }
}

if (!function_exists('is_admin')) {
    function is_admin()
    {
        if ($user = current_user()) {
            return ($user->admin);
        }

        return false;
    }
}

if (!function_exists('pre')) {
    function pre($val)
    {
        echo "<pre>" . print_r($val, true) . "</pre>";
    }
}

if (!function_exists('q')) {
    function q()
    {
        $queries = \DB::getQueryLog();
        pre(array_reverse($queries));
    }
}

if (!function_exists('cached_asset')) {
    function cached_asset($path, $bustQuery = true)
    {
        // Get the full path to the asset.
        $realPath = public_path($path);

        if (!file_exists($realPath)) {
            throw new LogicException("File not found at [{$realPath}]");
        }

        // Get the last updated timestamp of the file.
        $timestamp = filemtime($realPath);

        if (!$bustQuery) {
            // Get the extension of the file.
            $extension = pathinfo($realPath, PATHINFO_EXTENSION);

            // Strip the extension off of the path.
            $stripped = substr($path, 0, -(strlen($extension) + 1));

            // Put the timestamp between the filename and the extension.
            $path = implode('.', array($stripped, $timestamp, $extension));
        } else {
            // Append the timestamp to the path as a query string.
            $path .= '?' . $timestamp;
        }

        return asset($path);
    }
}