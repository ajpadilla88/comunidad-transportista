<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Camion extends Model
{
    protected $table = 'camiones';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
		'title',
		'slug',
		'price',
		'state_id',
		'aplicacion_uso_id',
		'fecha_venta_id',
		'modelo_camion_id',
		'anio',
		'tipo_camion_id',
		'traccion_camion_id',
		'peso_camion_id',
		'capacidad_carga_camion_id',
		'cilindrada_id',
		'potencia_id',
		'direccion_id',
		'kilometraje_id',
		'equipamiento_camion_id',
		'tanque_adicional_camion_id',
		'estado_cabina_id',
		'dormitorio',
		'altura_cabina_id',
		'color_cabina_id',
		'observaciones_cabina',
		'estado_carroceria_id',
		'marca_carroceria',
		'tipo_carroceria_id',
		'accesorios_carroceria',
		'largo_carroceria',
		'ancho_carroceria',
		'alto_carroceria',
		'medida_neumaticos',
		'llanta_neumaticos',
		'desgaste_neumatico_id',
		'recapado_neumaticos',
		'observaciones'
    ];

    /**
     *
     *-------------------- Relations
     *
     */
    public function location()
    {
        return $this->belongsTo(State::class, 'state_id');
    }

    public function applicationForUse()
    {
        return $this->belongsTo(AplicacionUsoCamion::class, 'aplicacion_uso_id');
    }



}
