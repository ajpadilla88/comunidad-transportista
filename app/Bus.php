<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bus extends Model
{
    public $table = 'buses';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
		'title',
		'slug',
		'price',
		'state_id',
		'aplicacion_uso_id',
		'fecha_venta_id',
		'modelo_id',
		'anio',
		'traccion_id',
		'peso_id',
		'transmision_id',
		'cilindrada_id',
		'potencia_id',
		'direccion_id',
		'kilometraje_id',
		'estado_carroceria_id',
		'marca_carroceria_id',
		'configuracion_carroceria_id',
		'observaciones_carroceria',
		'equipamiento_id',
		'tanque_adicional_id',
		'ancho_neumaticos',
		'alto_neumaticos',
		'diametro_llantas',
		'desgaste_neumatico_id',
		'recapado_neumaticos',
		'observaciones'

    ];


    
    /**
     *
     *-------------------- Relations
     *
     */
    public function location()
    {
        return $this->belongsTo(State::class, 'state_id');
    }

    public function applicationForUse()
    {
        return $this->belongsTo(AplicacionUsoBus::class, 'aplicacion_uso_id');
    }

}
