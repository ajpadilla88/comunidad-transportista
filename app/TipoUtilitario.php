<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoUtilitario extends Model
{
    public $table = 'tipo_utilitarios';
}
