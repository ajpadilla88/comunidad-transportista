<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModeloUtilitario extends Model
{
    public $table = 'modelo_utilitarios';
}
