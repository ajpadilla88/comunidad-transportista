<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EquipamientoBus extends Model
{
    public $table = 'equipamiento_buses';
}
