<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoBus extends Model
{
    public $table = 'tipo_buses';
}
