<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EquipamientoCamion extends Model
{
    public $table = 'equipamiento_camiones';
}
