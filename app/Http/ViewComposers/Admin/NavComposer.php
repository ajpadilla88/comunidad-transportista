<?php namespace App\Http\ViewComposers\Admin;

class NavComposer
{

    public function compose($view)
    {
        $first_name = current_user() ? current_user()->first_name : null;

        $messages = collect();
        $notifications = collect();

        $view->with(compact('first_name', 'messages', 'notifications'));
    }

}