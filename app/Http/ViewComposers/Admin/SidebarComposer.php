<?php namespace App\Http\ViewComposers\Admin;

use App\User;
use URL;

class SidebarComposer
{

    private $menu = [];

    public function compose($view)
    {
        $this->buildMenu();
        $this->setActiveItems();

        $view->with('primary_menu', $this->menu);
    }

    private function buildMenu()
    {
        $this->menu = [];

        $this->menu[] = ['title' => 'Dashboard', 'link' => '#', 'icon' => 'tachometer'];
//        $this->menu[] = ['title' => 'Landings', 'link' => route('admin.landings.index'), 'icon' => 'list'];
        $this->menu[] = ['title' => 'Activities', 'link' => route('admin.activities.index'), 'icon' => 'list'];
        $this->menu[] = ['title' => 'Actions', 'link' => route('admin.actions.index'), 'icon' => 'leaf'];
        $this->menu[] = ['title' => 'Habits', 'link' => route('admin.habits.index'), 'icon' => 'check-square-o'];
        $this->menu[] = ['title' => 'Users', 'link' => route('admin.users.index'), 'icon' => 'user'];
//        $this->menu[] = ['title' => 'Settings', 'link' => route('admin.settings.index'), 'icon' => 'cog'];
    }

    private function setActiveItems()
    {
        foreach ($this->menu as &$menu) {
            if ($menu['link'] && $menu['link'] == URL::current()) {
                $menu['active'] = true;
                break;
            } else {
                if (isset($menu['items'])) {
                    foreach ($menu['items'] as $item) {
                        if ($item['link'] == URL::current()) {
                            $menu['active'] = true;
                            break;
                        }
                    }
                }
            }
        }
    }
}


// Example
// [
//   'title' => 'Projects',
//   'link' => route('admin.projects.index'),
//   'items' => [
//     [
//       'link' => route('admin.projects.index'),
//       'title' => 'List all',
//       'icon' => 'list'
//     ],
//     [
//       'link' => route('admin.projects.create'),
//       'title' => 'Create',
//       'icon' => 'plus'
//     ],
//   ],
//   'icon' => 'book'
// ]