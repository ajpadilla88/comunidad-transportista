<?php namespace App\Http\ViewComposers\Account;

use URL;

class SidebarComposer
{

    private $menu = [];
    

    public function compose($view)
    {
        $this->buildMenu();
        $this->setActiveItems();

        $first_name = current_user() ? current_user()->first_name : null;

        $view->with('primary_menu', $this->menu)
        ->with('first_name',$first_name);
    }

    private function buildMenu()
    {
        $this->menu = [];

        $this->menu[] = ['title' => 'Mi Cuenta', 'link' => '#', 'icon' => 'tachometer'];
        $this->menu[] = ['title' => 'Anuncios publicados', 'link' => route('account.listings.index'), 'icon' => 'list'];
        $this->menu[] = ['title' => 'Mensajes', 'link' => route('account.messages.index'), 'icon' => 'leaf'];
       // $this->menu[] = ['title' => 'Mis favoritos', 'link' => route('account.favorites.index'), 'icon' => 'check-square-o'];
//        $this->menu[] = ['title' => 'Settings', 'link' => route('admin.settings.index'), 'icon' => 'cog'];
    }

    private function setActiveItems()
    {
        foreach ($this->menu as &$menu) {
            if ($menu['link'] && $menu['link'] == URL::current()) {
                $menu['active'] = true;
                break;
            } else {
                if (isset($menu['items'])) {
                    foreach ($menu['items'] as $item) {
                        if ($item['link'] == URL::current()) {
                            $menu['active'] = true;
                            break;
                        }
                    }
                }
            }
        }
    }
}


// Example
// [
//   'title' => 'Projects',
//   'link' => route('admin.projects.index'),
//   'items' => [
//     [
//       'link' => route('admin.projects.index'),
//       'title' => 'List all',
//       'icon' => 'list'
//     ],
//     [
//       'link' => route('admin.projects.create'),
//       'title' => 'Create',
//       'icon' => 'plus'
//     ],
//   ],
//   'icon' => 'book'
// ]