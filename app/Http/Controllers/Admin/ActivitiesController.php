<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\ActionRepository;
use App\Repositories\ActivityRepository;
use App\Repositories\UserRepository;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ActivitiesController extends Controller
{
    /** @var \App\Repositories\ActivityRepository */
    protected $activityRepository;

    /** @var \App\Repositories\ActionRepository */
    protected $actionRepository;

    /** @var \App\Repositories\UserRepository */
    protected $userRepository;

    public function __construct(ActivityRepository $activityRepository, ActionRepository $actionRepository, UserRepository $userRepository)
    {
        $this->activityRepository = $activityRepository;
        $this->actionRepository = $actionRepository;
        $this->userRepository = $userRepository;
    }

    public function index(Request $request)
    {
        $params = $request->all();
        $items = $this->activityRepository->search($params)->paginate(100);
        $actions = $this->actionRepository->all();

        return view('admin.activities.index', compact('items', 'params'))
            ->nest('table', 'admin.activities.table', compact('items', 'actions'));
    }

    public function create(Request $request)
    {
        $activity = $this->activityRepository->newInstance();
        $actions = $this->actionRepository->all();
        $users = $this->userRepository->all();

        return view()->make('admin.activities.create', compact('activity', 'actions', 'users'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'date' => 'date_format:d/m/Y H:i',
        ]);

        try {
            // Override Date
            $request->offsetSet('date', Carbon::createFromFormat('d/m/Y H:i', $request->get('date'))->toDateTimeString());

            DB::beginTransaction();
            $activity = $this->activityRepository->create($request->except('actions'));

            // Sync enabled clients
            $actions = collect($request->get('actions'))->filter(function ($item) {
                return isset($item['amount']) && intval($item['amount']) > 0;
            });
            $this->activityRepository->syncActions($activity, $actions);
            DB::commit();

            return redirect()->route('admin.activities.index')->with('alert_success', 'Item created successfully!');
        } catch (Exception $e) {
            DB::rollback();

            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function edit(Request $request, $id)
    {
        $activity = $this->activityRepository->getById($id);
        $actions = $this->actionRepository->all();
        $users = $this->userRepository->all();

        return view()->make('admin.activities.edit', compact('activity', 'actions', 'users'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'date' => 'date_format:d/m/Y H:i',
        ]);

        $activity = $this->activityRepository->getById($id);

        try {
            // Override Date
            $request->offsetSet('date', Carbon::createFromFormat('d/m/Y H:i', $request->get('date'))->toDateTimeString());

            DB::beginTransaction();
            $this->activityRepository->update($activity, $request->except('actions'));

            // Sync enabled clients
            $actions = collect($request->get('actions'))->filter(function ($item) {
                return isset($item['amount']) && intval($item['amount']) > 0;
            });


            $this->activityRepository->syncActions($activity, $actions);
            DB::commit();

            return redirect()->route('admin.activities.index')->with('alert_success', 'Item updated successfully!');
        } catch (Exception $e) {
            DB::rollback();

            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
}
