<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\MessageRepository;
use Illuminate\Http\Request;

class MessagesController extends Controller
{
    /** @var \App\Repositories\MessageRepository */
    protected $messageRepository;

    public function __construct(MessageRepository $messageRepository)
    {
        $this->messageRepository = $messageRepository;
    }

    public function index(Request $request)
    {
        $params = $request->all();
        $items = $this->messageRepository->search($params)->paginate(100);

        return view('admin.messages.index', compact('items', 'params'))
            ->nest('table', 'admin.messages.table', compact('items'));
    }
}
