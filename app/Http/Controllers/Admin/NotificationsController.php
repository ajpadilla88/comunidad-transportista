<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\NotificationRepository;
use Illuminate\Http\Request;

class NotificationsController extends Controller
{
    /** @var \App\Repositories\NotificationRepository */
    protected $notificationRepository;

    public function __construct(NotificationRepository $notificationRepository)
    {
        $this->notificationRepository = $notificationRepository;
    }

    public function index(Request $request)
    {
        $params = $request->all();
        $items = $this->notificationRepository->search($params)->paginate(100);

        return view('admin.notifications.index', compact('items', 'params'))
            ->nest('table', 'admin.notifications.table', compact('items'));
    }
}
