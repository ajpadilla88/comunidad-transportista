<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\HabitRepository;
use Illuminate\Http\Request;

class HabitsController extends Controller
{
    /** @var \App\Repositories\HabitRepository */
    protected $habitRepository;

    public function __construct(HabitRepository $habitRepository)
    {
        $this->habitRepository = $habitRepository;
    }

    public function index(Request $request)
    {
        $params = $request->all();
        $items = $this->habitRepository->search($params)->paginate(100);

        return view('admin.habits.index', compact('items', 'params'))
            ->nest('table', 'admin.habits.table', compact('items'));
    }
}
