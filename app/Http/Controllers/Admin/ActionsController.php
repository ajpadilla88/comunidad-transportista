<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\ActionRepository;
use Illuminate\Http\Request;

class ActionsController extends Controller
{
    /** @var \App\Repositories\ActionRepository */
    protected $actionRepository;

    public function __construct(ActionRepository $actionRepository)
    {
        $this->actionRepository = $actionRepository;
    }

    public function index(Request $request)
    {
        $params = $request->all();
        $items = $this->actionRepository->search($params)->paginate(100);

        return view('admin.actions.index', compact('items', 'params'))
            ->nest('table', 'admin.actions.table', compact('items'));
    }
}
