<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FileController extends Controller
{
    
	public function store(Request $request)
	 {
	            $path = public_path().'/uploads/';
	            $files = $request->file('file');
	            foreach($files as $file){
	                $fileName = $file->getClientOriginalName();
	                $file->move($path, $fileName);
	            }
	 }
}
