<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// Repositories
use \App\Repositories\BusRepository;
use \App\Repositories\CamionRepository;
use \App\Repositories\UtilitarioRepository;
use \App\Repositories\MarcaRepository;
use \App\Repositories\MarcaCarroceriaBusRepository;
use \App\Repositories\ModeloBusRepository;
use \App\Repositories\ModeloCamionRepository;
use \App\Repositories\ModeloUtilitarioRepository;
use \App\Repositories\KilometrajeRepository;
use \App\Repositories\AplicacionUsoBusRepository;
use \App\Repositories\AplicacionUsoCamionRepository;
use \App\Repositories\AplicacionUsoUtilitarioRepository;

use \App\Repositories\TipoCamionRepository;
use \App\Repositories\TipoBusRepository;
use \App\Repositories\TipoUtilitarioRepository;
use \App\Repositories\TraccionBusRepository;
use \App\Repositories\TraccionCamionRepository;
use \App\Repositories\TraccionUtilitarioRepository;
use \App\Repositories\PesoCamionRepository;
use \App\Repositories\PotenciaRepository;
use \App\Repositories\CilindradaRepository;
use \App\Repositories\TransmisionRepository;
use \App\Repositories\DireccionRepository;
use \App\Repositories\DesgasteNeumaticoRepository;
use \App\Repositories\EquipamientoBusRepository;
use \App\Repositories\EquipamientoCamionRepository;
use \App\Repositories\EquipamientoUtilitarioRepository;

use \App\Repositories\AlturaCabinaRepository;
use \App\Repositories\ColorCabinaRepository;
use \App\Repositories\FechaVentaRepository;
use \App\Repositories\StateRepository;

use \App\Repositories\CapacidadCargaCamionRepository;
use \App\Repositories\TanqueAdicionalCamionRepository;

use \App\Repositories\ConfiguracionCarroceriaBusRepository;



class VehicleController extends Controller
{


    /** @var \App\Repositories\BusRepository */
    protected $busRepository;

    /** @var \App\Repositories\MarcaRepository */
    protected $marcaRepository;

    /** @var \App\Repositories\MarcaCarroceriaBusRepository */
    protected $marcaCarroceriaBusRepository;

     /** @var \App\Repositories\ModeloBusRepository */
    protected $modeloBusRepository;

     /** @var \App\Repositories\ModeloCamionRepository */
    protected $modeloCamionRepository;

    /** @var \App\Repositories\CamionRepository */
    protected $camionRepository;

     /** @var \App\Repositories\ModeloUtilitarioRepository */
    protected $modeloUtilitarioRepository;

     /** @var \App\Repositories\kilometrajeRepository */
    protected $kilometrajeRepository;

    /** @var \App\Repositories\AplicacionUsoBusRepository */
    protected $aplicacionUsoBusRepository;

     /** @var \App\Repositories\AplicacionUsoCamionRepository */
    protected $aplicacionUsoCamionRepository;

     /** @var \App\Repositories\AplicacionUsoUtilitarioRepository */
    protected $aplicacionUsoUtilitarioRepository;

    /** @var \App\Repositories\TipoCamionRepository */
    protected $tipoCamionRepository;

    /** @var \App\Repositories\TipoBusRepository */
    protected $tipoBusRepository;

    /** @var \App\Repositories\TipoUtilitarioRepository */
    protected $tipoUtilitarioRepository;

    /** @var \App\Repositories\TraccionCamionRepository */
    protected $traccionCamionRepository;

    /** @var \App\Repositories\TraccionBusRepository */
    protected $traccionBusRepository;

    /** @var \App\Repositories\TraccionUtilitarioRepository */
    protected $traccionUtilitarioRepository;

     /** @var \App\Repositories\PesoCamionRepository */
    protected $pesoCamionRepository;

     /** @var \App\Repositories\PotenciaRepository */
    protected $potenciaRepository;

     /** @var \App\Repositories\CilindradaRepository */
    protected $cilindradaRepository;

     /** @var \App\Repositories\TransmisionRepository */
    protected $transmisionRepository;

    /** @var \App\Repositories\DireccionRepository */
    protected $direccionRepository;

     /** @var \App\Repositories\DesgasteNeumaticoRepository */
    protected $desgasteNeumaticoRepository;

     /** @var \App\Repositories\EquipamientoBusRepository */
    protected $equipamientoBusRepository;

    /** @var \App\Repositories\EquipamientoCamionRepository */
    protected $equipamientoCamionRepository;

    /** @var \App\Repositories\EquipamientoUtilitarioRepository */
    protected $equipamientoUtilitarioRepository;

     /** @var \App\Repositories\AlturaCabinaRepository */
    protected $alturaCabinaRepository;

     /** @var \App\Repositories\ColorCabinaRepository */
    protected $colorCabinaRepository;

     /** @var \App\Repositories\FechaVentaRepository */
    protected $fechaVentaRepository;

     /** @var \App\Repositories\StateRepository */
    protected $stateRepository;

    /** @var \App\Repositories\CapacidadCargaCamionRepository */
    protected $capacidadCargaCamionRepository;

    /** @var \App\Repositories\TanqueAdicionalCamionRepository */
    protected $tanqueAdicionalCamionRepository;

    /** @var \App\Repositories\ConfiguracionCarroceriaBusRepository */
    protected $configuracionCarroceriaBusRepository;

    /** @var \App\Repositories\UtilitarioRepository */
    protected $utilitarioRepository;

    


    public function __construct
    (
    	BusRepository $busRepository,
    	MarcaRepository $marcaRepository,
        MarcaCarroceriaBusRepository $marcaCarroceriaBusRepository,
        ModeloBusRepository $modeloBusRepository,
        ModeloCamionRepository $modeloCamionRepository,
        CamionRepository $camionRepository,
        ModeloUtilitarioRepository $modeloUtilitarioRepository,
        KilometrajeRepository $kilometrajeRepository,
        AplicacionUsoBusRepository $aplicacionUsoBusRepository,
        AplicacionUsoCamionRepository $aplicacionUsoCamionRepository,
        AplicacionUsoUtilitarioRepository $aplicacionUsoUtilitarioRepository,
        TipoBusRepository $tipoBusRepository,
        TipoCamionRepository $tipoCamionRepository,
        TipoUtilitarioRepository $tipoUtilitarioRepository,
        TraccionBusRepository $traccionBusRepository,
        TraccionCamionRepository $traccionCamionRepository,
        TraccionUtilitarioRepository $traccionUtilitarioRepository,
        PesoCamionRepository $pesoCamionRepository,
        PotenciaRepository $potenciaRepository,
        CilindradaRepository $cilindradaRepository,
        TransmisionRepository $transmisionRepository,
        DireccionRepository $direccionRepository,
        DesgasteNeumaticoRepository $desgasteNeumaticoRepository,
        EquipamientoBusRepository $equipamientoBusRepository,
        EquipamientoCamionRepository $equipamientoCamionRepository,
        EquipamientoUtilitarioRepository $equipamientoUtilitarioRepository,
        AlturaCabinaRepository $alturaCabinaRepository,
        ColorCabinaRepository $colorCabinaRepository,
        FechaVentaRepository $fechaVentaRepository,
        StateRepository $stateRepository,
        CapacidadCargaCamionRepository $capacidadCargaCamionRepository,
        TanqueAdicionalCamionRepository $tanqueAdicionalCamionRepository,
        ConfiguracionCarroceriaBusRepository $configuracionCarroceriaBusRepository,
        UtilitarioRepository $utilitarioRepository




    ){
        $this->middleware('auth');

        $this->busRepository = $busRepository;
        $this->marcaRepository = $marcaRepository;
        $this->marcaCarroceriaBusRepository = $marcaCarroceriaBusRepository;
        $this->modeloBusRepository = $modeloBusRepository;
        $this->modeloCamionRepository = $modeloCamionRepository;
        $this->camionRepository = $camionRepository;
        $this->modeloUtilitarioRepository = $modeloUtilitarioRepository;
        $this->kilometrajeRepository = $kilometrajeRepository;
        $this->aplicacionUsoBusRepository = $aplicacionUsoBusRepository;
        $this->aplicacionUsoCamionRepository = $aplicacionUsoCamionRepository;
        $this->aplicacionUsoUtilitarioRepository = $aplicacionUsoUtilitarioRepository;
        $this->tipoBusRepository = $tipoBusRepository;
        $this->tipoCamionRepository = $tipoCamionRepository;
        $this->tipoUtilitarioRepository = $tipoUtilitarioRepository;
        $this->traccionBusRepository = $traccionBusRepository;
        $this->traccionCamionRepository = $traccionCamionRepository;
        $this->traccionUtilitarioRepository = $traccionUtilitarioRepository;
        $this->pesoCamionRepository = $pesoCamionRepository;
        $this->potenciaRepository = $potenciaRepository;
        $this->cilindradaRepository = $cilindradaRepository;
        $this->transmisionRepository = $transmisionRepository;
        $this->direccionRepository = $direccionRepository;
        $this->desgasteNeumaticoRepository = $desgasteNeumaticoRepository;
        $this->equipamientoBusRepository = $equipamientoBusRepository;
        $this->equipamientoCamionRepository = $equipamientoCamionRepository;
        $this->equipamientoUtilitarioRepository = $equipamientoUtilitarioRepository;
        $this->alturaCabinaRepository = $alturaCabinaRepository;
        $this->colorCabinaRepository = $colorCabinaRepository;
        $this->fechaVentaRepository = $fechaVentaRepository;
        $this->stateRepository = $stateRepository;
        $this->capacidadCargaCamionRepository = $capacidadCargaCamionRepository;
        $this->tanqueAdicionalCamionRepository = $tanqueAdicionalCamionRepository;
        $this->configuracionCarroceriaBusRepository = $configuracionCarroceriaBusRepository;
        $this->utilitarioRepository = $utilitarioRepository;
        

    }


    public function index(){

        $marcas = $this->marcaRepository->search()->get();
        $kms    = $this->kilometrajeRepository->search()->get();
        $fechaVenta    = $this->fechaVentaRepository->search()->get();
    	$state    = $this->stateRepository->search()->get();


    	return view('account.vehicles.create', compact('marcas', 'kms', 'fechaVenta', 'state'));
    }

    public function store(Request $request){

        $input = $request->except('method', 'uri', 'ip', '_token');

        switch ($request->type) {
            case 1:

                $input['slug'] = \Str::slug($input['title']);
                $input['equipamiento_camion_id'] = 1; // Por definir
                $input['llanta_neumaticos'] = 'nose';
                $input['modelo_id'] = $input['modelo_camion_id'];
                $input['traccion_id'] = $input['traccion_camion_id'];
                $input['peso_id'] = $input['peso_camion_id'];

                $rules = [
                    'title' => 'required',
                    'slug' => 'required',
                    'price' => 'required', 
                    'state_id' => 'required |exists:states,id', 
                    'aplicacion_uso_id' => 'required |exists:aplicacion_uso_camiones,id', 
                    'modelo_id' => 'required |exists:modelo_camiones,id', 
                    'anio' => 'required', 
                    'traccion_camion_id' => 'required |exists:traccion_camiones,id', 
                    'peso_camion_id' => 'required |exists:peso_camiones,id', 
                    'transmision_id' => 'required |exists:tipo_camiones,id', 
                    'desgaste_neumatico_id' => 'required |exists:desgaste_neumaticos,id', 
                    'observaciones' => 'required'
                ];

                $validator = \Validator::make($input, $rules);

                if ($validator->fails()) {
                    return $validator->errors();
                }
                
                $this->busRepository->create($input);

                return response()->json(['cod' => 'WS001', 'message' => 'successfully saved.']);

                break;
            case 2:

                $input['slug'] = \Str::slug($input['title']);
                $input['equipamiento_camion_id'] = 1; // Por definir
                $input['llanta_neumaticos'] = 'nose';

                $rules = [
                    'title' => 'required',
                    'slug' => 'required',
                    'price' => 'required', 
                    'state_id' => 'required |exists:states,id', 
                    'aplicacion_uso_id' => 'required |exists:aplicacion_uso_camiones,id', 
                    'modelo_camion_id' => 'required |exists:modelo_camiones,id', 
                    'anio' => 'required', 
                    'tipo_camion_id' => 'required |exists:tipo_camiones,id', 
                    'traccion_camion_id' => 'required |exists:traccion_camiones,id', 
                    'peso_camion_id' => 'required |exists:peso_camiones,id', 
                    'capacidad_carga_camion_id' => 'required |exists:capacidad_carga_camiones,id', 
                    'estado_cabina_id' => 'required |exists:estados,id', 
                    'dormitorio' => 'required', 
                    'altura_cabina_id' => 'required |exists:altura_cabinas,id', 
                    'medida_neumaticos' => 'required', 
                    'llanta_neumaticos' => 'required', 
                    'desgaste_neumatico_id' => 'required |exists:desgaste_neumaticos,id', 
                    'observaciones' => 'required'
                ];

                $validator = \Validator::make($input, $rules);

                if ($validator->fails()) {
                    return $validator->errors();
                }
                
                $this->camionRepository->create($input);

                return response()->json(['cod' => 'WS001', 'message' => 'successfully saved.']);

                break;
            case 3:
                $input['slug'] = \Str::slug($input['title']);
                $input['equipamiento_camion_id'] = 1; // Por definir
                $input['llanta_neumaticos'] = 'nose';
                $input['modelo_id'] = $input['modelo_camion_id'];
                $input['traccion_id'] = $input['traccion_camion_id'];
                $input['peso_id'] = $input['peso_camion_id'];
                $input['tipo_id'] = $input['tipo_camion_id'];
                $input['capacidad_carga_id'] = $input['capacidad_carga_camion_id'];
                $input['estado_id'] = $input['estado_carroceria_id'];
                $input['altura_cabina_id'] = $input['techo'];

                $rules = [
                    'title' => 'required',
                    'slug' => 'required',
                    'price' => 'required', 
                    'state_id' => 'required |exists:states,id', 
                    'aplicacion_uso_id' => 'required |exists:aplicacion_uso_camiones,id', 
                    'modelo_id' => 'required |exists:modelo_camiones,id', 
                    'anio' => 'required', 
                    'traccion_camion_id' => 'required |exists:traccion_camiones,id', 
                    'peso_camion_id' => 'required |exists:peso_camiones,id', 
                    'transmision_id' => 'required |exists:tipo_camiones,id', 
                    'desgaste_neumatico_id' => 'required |exists:desgaste_neumaticos,id', 
                    'observaciones' => 'required'
                ];

                $validator = \Validator::make($input, $rules);

                if ($validator->fails()) {
                    return $validator->errors();
                }
                
                $this->utilitarioRepository->create($input);

                return response()->json(['cod' => 'WS001', 'message' => 'successfully saved.']);
                break;
            
            default:
                # code...
                break;
        }


        return view('account.vehicles.create');
    }

    

    public function filter(Request $request){

        if($request->ajax()){

            if(isset($request->type)){
                switch ($request->type) {
                    case 1:
                        $marcas = $this->marcaRepository->search()->get();
                        break;
                    default:
                        $marcas = $this->marcaRepository->search()->get();
                        break;
                }
            }

            if(isset($request->marca)){
                switch ($request->type) {
                    case 1:
                        $modelos = $this->modeloBusRepository->getByMarca($request->marca)->get();
                        break;
                    case 2:
                        $modelos = $this->modeloCamionRepository->getByMarca($request->marca)->get();
                        break;
                    case 3:
                        $modelos = $this->modeloUtilitarioRepository->getByMarca($request->marca)->get();
                        break;
                    default:
                        $modelos = [];
                        break;
                }
            }

            if(isset($request->modelo)){
                switch ($request->type) {
                    case 1:
                        $aplicacionUso = $this->aplicacionUsoBusRepository->search()->get();
                        break;
                    case 2:
                        $aplicacionUso = $this->aplicacionUsoCamionRepository->search()->get();
                        break;
                    case 3:
                        $aplicacionUso = $this->aplicacionUsoUtilitarioRepository->search()->get();
                        break;
                    default:
                        $aplicacionUso = [];
                        break;
                }
            }

            if(isset($request->step)){

                $params = [];

                switch ($request->type) {
                    case 1:
                        $params['tipo'] = $this->tipoCamionRepository->search()->get();
                        $params['traccion'] = $this->traccionCamionRepository->search()->get();
                        $params['peso'] = $this->pesoCamionRepository->search()->get();
                        $params['potencia'] = $this->potenciaRepository->search()->get();
                        $params['cilindrada'] = $this->cilindradaRepository->search()->get();
                        $params['transmision'] = $this->transmisionRepository->search()->get();
                        $params['direccion'] = $this->direccionRepository->search()->get();

                        $params['neumatico'] = $this->desgasteNeumaticoRepository->search()->get();
                        $params['equipamiento'] = $this->equipamientoCamionRepository->search()->get();
                        $params['configuracion'] = $this->configuracionCarroceriaBusRepository->search()->get();
                        $params['marcaCarroceria'] = $this->marcaCarroceriaBusRepository->search()->get();

                        break;
                    case 2:
                        $params['aplicacion'] = $this->aplicacionUsoCamionRepository->search()->get();
                        $params['tipo'] = $this->tipoCamionRepository->search()->get();
                        $params['traccion'] = $this->traccionCamionRepository->search()->get();
                        $params['peso'] = $this->pesoCamionRepository->search()->get();
                        $params['cilindrada'] = $this->cilindradaRepository->search()->get();
                        $params['transmision'] = $this->transmisionRepository->search()->get();
                        $params['direccion'] = $this->direccionRepository->search()->get();
                        $params['neumatico'] = $this->desgasteNeumaticoRepository->search()->get();
                        $params['equipamiento'] = $this->equipamientoCamionRepository->search()->get();
                        $params['potencia'] = $this->potenciaRepository->search()->get();
                        $params['altura'] = $this->alturaCabinaRepository->search()->get();
                        $params['color'] = $this->colorCabinaRepository->search()->get();
                        $params['fecha'] = $this->fechaVentaRepository->search()->get();
                        $params['state'] = $this->stateRepository->search()->get();
                        $params['carga'] = $this->capacidadCargaCamionRepository->search()->get();
                        $params['tanque'] = $this->tanqueAdicionalCamionRepository->search()->get();
                        break;
                    case 3:
                        $params['tipo'] = $this->tipoCamionRepository->search()->get();
                        $params['traccion'] = $this->traccionCamionRepository->search()->get();
                        $params['peso'] = $this->pesoCamionRepository->search()->get();
                        $params['potencia'] = $this->potenciaRepository->search()->get();
                        $params['cilindrada'] = $this->cilindradaRepository->search()->get();
                        $params['transmision'] = $this->transmisionRepository->search()->get();
                        $params['direccion'] = $this->direccionRepository->search()->get();
                        $params['altura'] = $this->alturaCabinaRepository->search()->get();
                        $params['neumatico'] = $this->desgasteNeumaticoRepository->search()->get();
                        $params['equipamiento'] = $this->equipamientoCamionRepository->search()->get();
                        $params['carga'] = $this->capacidadCargaCamionRepository->search()->get();

                        break;
                    default:
                        $aplicacionUso = [];
                        break;
                }
            }

            return response()->json(compact('marcas', 'modelos', 'aplicacionUso', 'params'), 200);

        }	
    }
}
