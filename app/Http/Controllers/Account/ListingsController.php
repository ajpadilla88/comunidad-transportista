<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ListingsController extends Controller
{

    public function __construct()
    {
    }

    public function index(Request $request)
    {
        $params = $request->all();
        $items = collect();

        return view('account.listings.index', compact('items', 'params'))
            ->nest('table', 'account.listings.table', compact('items'));
    }
}
