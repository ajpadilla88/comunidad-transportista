<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class NotificationsController extends Controller
{

    public function __construct()
    {
    }

    public function index(Request $request)
    {
        $params = $request->all();

        return view('account.notifications.index', compact('items', 'params'))
            ->nest('table', 'account.notifications.table', compact('items'));
    }
}
