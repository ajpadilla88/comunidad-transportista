<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use App\Repositories\MessageRepository;
use Illuminate\Http\Request;

class MessagesController extends Controller
{

    public function __construct()
    {
    }

    public function index(Request $request)
    {
        $params = $request->all();
        $items = collect();

        return view('account.messages.index', compact('items', 'params'))
            ->nest('table', 'account.messages.table', compact('items'));
    }
}
