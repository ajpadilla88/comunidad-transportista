<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Repositories\UserRepository;
use \App\Repositories\StateRepository;
use \App\Repositories\ConcesionarioRepository;

class UserController extends Controller
{
   /** @var \App\Repositories\UserRepository */
   protected $repository;

   /** @var \App\Repositories\StateRepository */
   protected $stateRepository;

   /** @var \App\Repositories\ConcesionarioRepository */
   protected $concesionarioRepository;

   public function __construct(
   		UserRepository $repository,
   		StateRepository $stateRepository,
   		ConcesionarioRepository $concesionarioRepository
   ){
   		$this->repository = $repository;
   		$this->stateRepository = $stateRepository;
   		$this->concesionarioRepository = $concesionarioRepository;
   }

   public function show()
   {
   		$user = \Auth::user();
   		$states = $this->stateRepository->all()->pluck('name', 'id');
   		$concesionarios = $this->concesionarioRepository->all()->pluck('name', 'id');
   		return view('account.profiles.edit', compact('user', 'states', 'concesionarios'));

   }

   public function update(Request $request, $id)
   {
	   	$input = $request->all();
	   	$input['id'] = $id;

	   	$rules = [
	   		'concesionario_id' => 'integer|exists:concesionarios,id',
	   		'first_name' => 'required',
	   		'last_name' => 'required', 
	   		'email' => 'required', 
	   		'phone' => '',
	   		'state_id' => 'integer|exists:states,id',
	   		'postal_code' => 'required',
	   	];

	   	if(!empty($input['password']) || !empty($input['password_confirmation'])) {
	   		$rules['password'] = 'sometimes|required|alpha_num|min:6|confirmed';
	   		$rules['password_confirmation'] = 'sometimes|required|alpha_num|min:6';
	   	}else{
	   		unset($input['password']);
	   		unset($input['password_confirmation']);
	   	}

	   	$validator = \Validator::make($input, $rules);

	   	if ($validator->fails()) {
	   		return redirect()->route('account.user.profile.show')
	   		->withErrors($validator)
	   		->withInput();
	   	}

	   	if (isset($input['password'])) {
	   		$input['password'] = \Hash::make($input['password']);
	   	}

	   	$user = $this->repository->getById($id);

	   	
	   	$this->repository->update($user, $input);

	   	\Session::flash('message_success','successfully saved.');

	   	return redirect()->route('account.user.profile.show');
   }

}
