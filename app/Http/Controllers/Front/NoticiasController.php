<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class NoticiasController extends Controller
{
    public function index()
    {
        return view('front.noticias.index');
    }

    public function show()
    {
        return view('front.noticias.show');
    }
}
