<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\StateRepository;
use App\Repositories\AplicacionUsoBusRepository;
use App\Repositories\BusRepository;

class BusesController extends Controller
{

	/** @var \App\Repositories\StateRepository */	
	protected $stateRepository;

	/** @var \App\Repositories\AplicacionUsoCamionRepository */	
	protected $AplicacionUsoCamionRepository;

	/** @var \App\Repositories\CamionRepository */	
	protected $busRepository;

	public function __construct(
		StateRepository $stateRepository,
		AplicacionUsoBusRepository $aplicacionUsoBusRepository,
		BusRepository $busRepository
	){
		$this->stateRepository = $stateRepository;
		$this->aplicacionUsoBusRepository = $aplicacionUsoBusRepository;
		$this->busRepository = $busRepository;
	}

    public function index()
    {
    	$states = $this->stateRepository->all()->pluck('name', 'id');
    	$applicationForUse = $this->aplicacionUsoBusRepository->all()->pluck('name', 'id');
    	$buses = $this->busRepository->all();
        return view('front.buses.index', compact('states', 'applicationForUse','buses'));
    }

    public function show()
    {
        return view('front.buses.show');
    }

    public function search(Request $request) {

    	if ($request->ajax()) {
    		$results = $this->busRepository->filter($request->all(),false, 10);
    		return response()->json(['data' => $results], 200);
    	}else{
    		$states = $this->stateRepository->all()->pluck('name', 'id');
    		$applicationForUse = $this->aplicacionUsoBusRepository->all()->pluck('name', 'id');
    		$buses = $this->busRepository->filter($request->all(),false, 10);
    		return view('front.buses.index', compact('states', 'applicationForUse','buses'));
    	}

    }
}
