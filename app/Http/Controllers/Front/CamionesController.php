<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\StateRepository;
use App\Repositories\AplicacionUsoCamionRepository;
use App\Repositories\CamionRepository;

class CamionesController extends Controller
{
	/** @var \App\Repositories\StateRepository */	
	protected $stateRepository;

	/** @var \App\Repositories\AplicacionUsoCamionRepository */	
	protected $AplicacionUsoCamionRepository;

	/** @var \App\Repositories\CamionRepository */	
	protected $camionRepository;

	public function __construct(
		StateRepository $stateRepository,
		AplicacionUsoCamionRepository $aplicacionUsoCamionRepository,
		CamionRepository $camionRepository
	){
		$this->stateRepository = $stateRepository;
		$this->aplicacionUsoCamionRepository = $aplicacionUsoCamionRepository;
		$this->camionRepository = $camionRepository;
	}

    public function index()
    {
    	$states = $this->stateRepository->all()->pluck('name', 'id');
    	$applicationForUse = $this->aplicacionUsoCamionRepository->all()->pluck('name', 'id');
    	$trucks = $this->camionRepository->all();
        return view('front.camiones.index', compact('states', 'applicationForUse', 'trucks'));
    }

    public function show()
    {
        return view('front.camiones.show');
    }

    public function search(Request $request) {

    	if ($request->ajax()) {
    		$results = $this->camionRepository->filter($request->all(),false, 10);
    		return response()->json(['data' => $results], 200);
    	}else{
    		$states = $this->stateRepository->all()->pluck('name', 'id');
    		$applicationForUse = $this->aplicacionUsoCamionRepository->all()->pluck('name', 'id');
    		$trucks = $this->camionRepository->filter($request->all(),false, 10);
    		//dd($trucks);
    		return view('front.camiones.index', compact('states', 'applicationForUse','trucks'));
    	}

    }
}
