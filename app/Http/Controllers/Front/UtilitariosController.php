<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\StateRepository;
use App\Repositories\AplicacionUsoUtilitarioRepository;
use App\Repositories\UtilitarioRepository;

class UtilitariosController extends Controller
{

	/** @var \App\Repositories\StateRepository */
    protected $stateRepository;

    /** @var \App\Repositories\AplicacionUsoUtilitarioRepository */
    protected $aplicacionUsoUtilitarioRepository;

    /** @var \App\Repositories\AplicacionUsoUtilitarioRepository */
    protected $utilitarioRepository;


    public function __construct(
    	StateRepository $stateRepository,
    	AplicacionUsoUtilitarioRepository $aplicacionUsoUtilitarioRepository,
    	UtilitarioRepository $utilitarioRepository
    ){
    	$this->stateRepository = $stateRepository;
    	$this->aplicacionUsoUtilitarioRepository = $aplicacionUsoUtilitarioRepository;
    	$this->utilitarioRepository = $utilitarioRepository;
    }

    public function index()
    {
    	$states = $this->stateRepository->all()->pluck('name', 'id');
    	$applicationForUse = $this->aplicacionUsoUtilitarioRepository->all()->pluck('name', 'id');
    	$utilities = $this->utilitarioRepository->all();
        return view('front.utilitarios.index', compact('states', 'applicationForUse', 'utilities'));
    }

    public function show()
    {
        return view('front.utilitarios.show');
    }

    public function search(Request $request) {

    	if ($request->ajax()) {
    		$results = $this->utilitarioRepository->filter($request->all(),false, 10);
    		return response()->json(['data' => $results], 200);
    	}else{
    		$states = $this->stateRepository->all()->pluck('name', 'id');
    		$applicationForUse = $this->aplicacionUsoUtilitarioRepository->all()->pluck('name', 'id');
    		$utilities = $this->utilitarioRepository->filter($request->all(),false, 10);
    		return view('front.utilitarios.index', compact('states', 'applicationForUse','utilities'));
    	}

    }

}
