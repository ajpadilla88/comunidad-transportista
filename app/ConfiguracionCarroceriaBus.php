<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConfiguracionCarroceriaBus extends Model
{
    public $table = 'configuracion_carroceria_buses';
}
