<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarcaCarroceriaBus extends Model
{
    public $table = 'marca_carroceria_buses';
}
