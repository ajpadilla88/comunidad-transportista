<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoCamion extends Model
{
    public $table = 'tipo_camiones';
}
