<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Utilitario extends Model
{
	public $table = 'utilitarios';

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
		'title',
		'slug',
		'price',
		'state_id',
		'aplicacion_uso_id',
		'fecha_venta_id',
		'modelo_id',
		'tipo_id',
		'traccion_id',
		'peso_id',
		'capacidad_carga_id',
		'cilindrada_id',
		'potencia_id',
		'direccion_id',
		'anio',
		'kilometraje_id',
		'equipamiento_id',
		'observaciones_equipamiento',
		'estado_id',
		'altura_cabina_id',
		'color_cabina_id',
		'observaciones_cabina',
		'ancho_neumaticos',
		'alto_neumaticos',
		'diametro_llantas',
		'desgaste_neumatico_id',
		'recapado_neumaticos',
		'observaciones'

    ];


    public function location()
    {
        return $this->belongsTo(State::class, 'state_id');
    }

    public function applicationForUse()
    {
        return $this->belongsTo(AplicacionUsoUtilitario::class, 'aplicacion_uso_id');
    }


}