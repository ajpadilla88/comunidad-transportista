<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoCarroceria extends Model
{
    public $table = 'tipo_carrocerias';
}
