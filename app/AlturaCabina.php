<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlturaCabina extends Model
{
    public $table = 'altura_cabinas';
}
