<?php

use Illuminate\Database\Seeder;

class TrucksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    
    	\DB::statement('SET FOREIGN_KEY_CHECKS=0;');
            DB::table('buses')->truncate();
            $titles = [
             'AGRALE 6000',
             'Bedford',
             'Chevrolet 714',
             'DFM C125',
             'DIMEX D 400',
             'Dodge DP 1000',
             'Fiat 160',
             'FORD CARGO 1119',
             'HINO 300 - 616',
             'HYUNDAI HD 78'
            ];

            $prices = [
                '105.000',
                '150.000',
                '200.000',
                '10.000',
                '105.000',
                '230.000',
                '95.050',
                '330.360',
                '200.300',
                '200.000',
            ];

            $years = [
                '2017',
                '2001',
                '2002',
                '2003',
                '2014',
                '2005',
                '2006',
                '1999',
                '2008',
                '2009',
                '2010'
            ];

            for ($i=0; $i < count($titles); $i++) { 
                DB::table('camiones')->insert([
                    'title' => $titles[$i], 
                    'slug' => 'text',
                    'price' => $prices[$i],
                    'state_id' => App\State::all()->random()->id,
                    'aplicacion_uso_id' => App\AplicacionUsoCamion::all()->random()->id,
                    'modelo_camion_id' => App\ModeloCamion::all()->random()->id,
                    'anio' => $years[$i], 
                    'tipo_camion_id' => App\TipoCamion::all()->random()->id,
                    'traccion_camion_id' => App\TraccionCamion::all()->random()->id, 
                    'peso_camion_id' => App\PesoCamion::all()->random()->id,
                    'capacidad_carga_camion_id' => App\CapacidadCargaCamion::all()->random()->id,
                    'estado_cabina_id' =>  App\Estado::all()->random()->id,
                    'dormitorio' => 1,
                    'altura_cabina_id' => App\AlturaCabina::all()->random()->id,
                    'medida_neumaticos' => '4X4X4',
                    'llanta_neumaticos' => 'llantas',
                    'desgaste_neumatico_id' => App\DesgasteNeumatico::all()->random()->id,
                    'recapado_neumaticos' => 1,
                    'observaciones' => 'text',
                ]);
            }
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    }
}
