<?php

use Illuminate\Database\Seeder;

class DealersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       \Eloquent::unguard();

        $dealers = [ 
        	'Hidalgo Motors', 
        	'Camiones Torbes', 
        	'Alconsa',
        	'Central Motors',
        	'Grecco Motors'
        ];

        $length = count($dealers);

        for ($i=0; $i < $length; $i++) { 
        	 DB::table('concesionarios')->insert(['name' => $dealers[$i]]);
        }
    }
}
