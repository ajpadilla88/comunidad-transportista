<?php

use Illuminate\Database\Seeder;

class BusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	\DB::statement('SET FOREIGN_KEY_CHECKS=0;');
    		DB::table('buses')->truncate();
    		for ($i=0; $i < ; $i++) { 
    			DB::table('buses')->insert([
    				'title' => '', 
    				'slug' => '',
    				'price' => '',
    				'state_id' => '',
    				'aplicacion_uso_id' => '',
    				'modelo_id' =>,
    				'anio' => '', 
    				'traccion_id' =>, 
    				'peso_id' =>,
    				'transmision_id' =>,
    				'desgaste_neumatico_id' => ,
    				'recapado_neumaticos' =>,
    				'observaciones' => ,
    			]);
    		}
    	\DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    }
}
