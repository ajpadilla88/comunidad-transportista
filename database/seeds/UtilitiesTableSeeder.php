<?php

use Illuminate\Database\Seeder;

class UtilitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	\DB::statement('SET FOREIGN_KEY_CHECKS=0;');
            DB::table('utilitarios')->truncate();

            $titles = [
             'IVECO DAILY 35C14',
             'JMC N900 4.0T',
             'KENWORTH T2000',
             'MACK Axle Back',
             'DIMEX D 400',
             'Mercedes Benz 712',
             'PETERBILT 378',
             'RENAULT TRUCKS C 320',
             'Scania 124 L',
             'Volkswagen Constellation 19.330'
            ];

            $prices = [
                '105.000',
                '150.000',
                '200.000',
                '10.000',
                '125.000',
                '230.000',
                '900.050',
                '330.360',
                '400.300',
                '500.000',
            ];

            $years = [
                '2017',
                '2001',
                '2002',
                '2003',
                '2014',
                '2005',
                '2006',
                '1999',
                '2008',
                '2009',
                '2010'
            ];

            for ($i=0; $i < count($titles); $i++) { 
                DB::table('utilitarios')->insert([
                    'title' => $titles[$i], 
                    'slug' => 'text',
                    'price' => $prices[$i],
                    'state_id' => App\State::all()->random()->id,

                    'aplicacion_uso_id' => App\AplicacionUsoUtilitario::all()->random()->id,

                    'modelo_id' => App\ModeloUtilitario::all()->random()->id,
                    'anio' => $years[$i], 
                    'tipo_id' => App\TipoUtilitario::all()->random()->id,
                    'traccion_id' => App\TraccionUtilitario::all()->random()->id, 
                    'peso_id' => App\PesoUtilitario::all()->random()->id,
                    'capacidad_carga_id' => App\CapacidadCargaUtilitario::all()->random()->id,
                    'estado_id' =>  App\Estado::all()->random()->id,
                    'altura_cabina_id' => App\AlturaCabina::all()->random()->id,
                    'recapado_neumaticos' => 1,
                    'observaciones' => 'text',
                ]);
            }
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
