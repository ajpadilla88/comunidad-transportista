<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // App\AplicacionUso
        Schema::create('aplicacion_uso_camiones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->softDeletes();
        });

        Schema::create('aplicacion_uso_buses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->softDeletes();
        });

        Schema::create('aplicacion_uso_utilitarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->softDeletes();
        });

        // App\Marca
        Schema::create('marcas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->softDeletes();
        });

        // App\ModeloCamion
        Schema::create('modelo_camiones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('marca_id')->unsigned();
            $table->string('name');
            $table->foreign('marca_id')->references('id')->on('marcas')->onUpdate('cascade')->onDelete('cascade');
            $table->softDeletes();
        });

        // App\ModeloBus
        Schema::create('modelo_buses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('marca_id')->unsigned();
            $table->string('name');
            $table->foreign('marca_id')->references('id')->on('marcas')->onUpdate('cascade')->onDelete('cascade');
            $table->softDeletes();
        });

        Schema::create('modelo_utilitarios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('marca_id')->unsigned();
            $table->string('name');
            $table->foreign('marca_id')->references('id')->on('marcas')->onUpdate('cascade')->onDelete('cascade');
            $table->softDeletes();
        });

        // App\TipoCamion
        Schema::create('tipo_camiones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->softDeletes();
        });
        Schema::create('tipo_buses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->softDeletes();
        });
        Schema::create('tipo_utilitarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->softDeletes();
        });
        Schema::create('traccion_camiones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->softDeletes();
        });
        Schema::create('traccion_buses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->softDeletes();
        });
        Schema::create('traccion_utilitarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->softDeletes();
        });
        Schema::create('peso_camiones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->softDeletes();
        });
        Schema::create('peso_utilitarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->softDeletes();
        });
        Schema::create('capacidad_carga_camiones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->softDeletes();
        });
        Schema::create('capacidad_carga_utilitarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->softDeletes();
        });
        Schema::create('cilindradas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->softDeletes();
        });
        Schema::create('potencias', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->softDeletes();
        });
        Schema::create('potencia_utilitarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->softDeletes();
        });
        Schema::create('direcciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->softDeletes();
        });
        Schema::create('kilometrajes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->softDeletes();
        });
        Schema::create('equipamiento_camiones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->softDeletes();
        });
        Schema::create('equipamiento_buses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->softDeletes();
        });
        Schema::create('equipamiento_utilitarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->softDeletes();
        });
        Schema::create('tanque_adicional_camiones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->softDeletes();
        });
        Schema::create('estados', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->softDeletes();
        });
        Schema::create('altura_cabinas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->softDeletes();
        });
        Schema::create('color_cabinas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->softDeletes();
        });
        Schema::create('tipo_carrocerias', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->softDeletes();
        });
        Schema::create('marca_carroceria_buses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->softDeletes();
        });
        Schema::create('configuracion_carroceria_buses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->softDeletes();
        });
        Schema::create('transmisiones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->softDeletes();
        });
        Schema::create('desgaste_neumaticos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->softDeletes();
        });
        Schema::create('fecha_ventas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        Schema::create('camiones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->decimal('price');
            $table->integer('state_id')->unsigned();
            $table->integer('aplicacion_uso_id')->unsigned();
            $table->integer('fecha_venta_id')->unsigned()->nullable();

            // Unidad
            $table->integer('modelo_camion_id')->unsigned();
            $table->smallInteger('anio');
            $table->integer('tipo_camion_id')->unsigned();
            $table->integer('traccion_camion_id')->unsigned();
            $table->integer('peso_camion_id')->unsigned();
            $table->integer('capacidad_carga_camion_id')->unsigned();
            $table->integer('cilindrada_id')->unsigned()->nullable();
            $table->integer('potencia_id')->unsigned()->nullable();
            $table->integer('direccion_id')->unsigned()->nullable();
            $table->integer('kilometraje_id')->unsigned()->nullable();

            // Equipamiento
            $table->integer('equipamiento_camion_id')->unsigned()->nullable();
            $table->integer('tanque_adicional_camion_id')->unsigned()->nullable();

            // Cabina
            $table->integer('estado_cabina_id')->unsigned();
            $table->boolean('dormitorio')->default(false);
            $table->integer('altura_cabina_id')->unsigned();
            $table->integer('color_cabina_id')->unsigned()->nullable();
            $table->string('observaciones_cabina')->nullable()->nullable();

            // Carroceria
            $table->integer('estado_carroceria_id')->unsigned()->nullable();
            $table->string('marca_carroceria')->nullable();
            $table->integer('tipo_carroceria_id')->unsigned()->nullable();
            $table->text('accesorios_carroceria')->nullable()->nullable();
            $table->decimal('largo_carroceria')->nullable();
            $table->decimal('ancho_carroceria')->nullable();
            $table->decimal('alto_carroceria')->nullable();

            // Neumaticos
            $table->string('medida_neumaticos');
            $table->string('llanta_neumaticos');
            $table->integer('desgaste_neumatico_id')->unsigned();
            $table->boolean('recapado_neumaticos')->default(false);

            // Extra
            $table->text('observaciones');

            // Foreigns

            $table->foreign('state_id')->references('id')->on('states')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('aplicacion_uso_id')->references('id')->on('aplicacion_uso_camiones')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('fecha_venta_id')->references('id')->on('fecha_ventas')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('modelo_camion_id')->references('id')->on('modelo_camiones')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('tipo_camion_id')->references('id')->on('tipo_camiones')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('traccion_camion_id')->references('id')->on('traccion_camiones')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('peso_camion_id')->references('id')->on('peso_camiones')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('capacidad_carga_camion_id')->references('id')->on('capacidad_carga_camiones')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('cilindrada_id')->references('id')->on('cilindradas')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('potencia_id')->references('id')->on('potencias')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('direccion_id')->references('id')->on('direcciones')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('kilometraje_id')->references('id')->on('kilometrajes')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('equipamiento_camion_id')->references('id')->on('equipamiento_camiones')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('tanque_adicional_camion_id')->references('id')->on('tanque_adicional_camiones')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('estado_cabina_id')->references('id')->on('estados')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('altura_cabina_id')->references('id')->on('altura_cabinas')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('color_cabina_id')->references('id')->on('color_cabinas')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('estado_carroceria_id')->references('id')->on('estados')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('tipo_carroceria_id')->references('id')->on('tipo_carrocerias')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('desgaste_neumatico_id')->references('id')->on('desgaste_neumaticos')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('buses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->decimal('price');
            $table->integer('state_id')->unsigned();
            $table->integer('aplicacion_uso_id')->unsigned();
            $table->integer('fecha_venta_id')->unsigned()->nullable();

            // Unidad
            $table->integer('modelo_id')->unsigned();
            $table->smallInteger('anio');
            $table->integer('traccion_id')->unsigned();
            $table->integer('peso_id')->unsigned();
            $table->integer('transmision_id')->unsigned();
            $table->integer('cilindrada_id')->unsigned()->nullable();
            $table->integer('potencia_id')->unsigned()->nullable();
            $table->integer('direccion_id')->unsigned()->nullable();
            $table->integer('kilometraje_id')->unsigned()->nullable();

            // Carroceria
            $table->integer('estado_carroceria_id')->unsigned()->nullable();
            $table->integer('marca_carroceria_id')->unsigned()->nullable();
            $table->integer('configuracion_carroceria_id')->unsigned()->nullable();
            $table->text('observaciones_carroceria')->nullable()->nullable();

            // Equipamiento
            $table->integer('equipamiento_id')->unsigned()->nullable();
            $table->integer('tanque_adicional_id')->unsigned()->nullable();

            // Neumaticos
            $table->integer('ancho_neumaticos')->nullable();
            $table->integer('alto_neumaticos')->nullable();
            $table->integer('diametro_llantas')->nullable();
            $table->integer('desgaste_neumatico_id')->unsigned();
            $table->boolean('recapado_neumaticos')->default(false);

            // Extra
            $table->text('observaciones');

            // Foreigns
            $table->foreign('state_id')->references('id')->on('states')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('aplicacion_uso_id')->references('id')->on('aplicacion_uso_buses')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('fecha_venta_id')->references('id')->on('fecha_ventas')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('modelo_id')->references('id')->on('modelo_buses')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('traccion_id')->references('id')->on('traccion_buses')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('peso_id')->references('id')->on('peso_camiones')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('transmision_id')->references('id')->on('transmisiones')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('cilindrada_id')->references('id')->on('cilindradas')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('potencia_id')->references('id')->on('potencias')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('direccion_id')->references('id')->on('direcciones')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('kilometraje_id')->references('id')->on('kilometrajes')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('estado_carroceria_id')->references('id')->on('estados')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('marca_carroceria_id')->references('id')->on('marca_carroceria_buses')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('configuracion_carroceria_id')->references('id')->on('configuracion_carroceria_buses')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('equipamiento_id')->references('id')->on('equipamiento_buses')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('tanque_adicional_id')->references('id')->on('tanque_adicional_camiones')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('desgaste_neumatico_id')->references('id')->on('desgaste_neumaticos')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('utilitarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->decimal('price');
            $table->integer('state_id')->unsigned();
            $table->integer('aplicacion_uso_id')->unsigned();
            $table->integer('fecha_venta_id')->unsigned()->nullable();

            // Unidad
            $table->integer('modelo_id')->unsigned();
            $table->integer('tipo_id')->unsigned();
            $table->integer('traccion_id')->unsigned();
            $table->integer('peso_id')->unsigned();
            $table->integer('capacidad_carga_id')->unsigned();
            $table->integer('cilindrada_id')->unsigned()->nullable();
            $table->integer('potencia_id')->unsigned()->nullable();
            $table->integer('direccion_id')->unsigned()->nullable();
            $table->smallInteger('anio');
            $table->integer('kilometraje_id')->unsigned()->nullable();

            // Equipamiento
            $table->integer('equipamiento_id')->unsigned()->nullable();
            $table->text('observaciones_equipamiento')->nullable();

            // Cabina
            $table->integer('estado_id')->unsigned();
            $table->integer('altura_cabina_id')->unsigned();
            $table->integer('color_cabina_id')->unsigned()->nullable();
            $table->string('observaciones_cabina')->nullable();

            // Neumaticos
            $table->integer('ancho_neumaticos')->nullable();
            $table->integer('alto_neumaticos')->nullable();
            $table->integer('diametro_llantas')->nullable();
            $table->integer('desgaste_neumatico_id')->unsigned()->nullable();
            $table->boolean('recapado_neumaticos')->default(false);

            // Extra
            $table->text('observaciones');

            // Foreigns
            $table->foreign('state_id')->references('id')->on('states')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('aplicacion_uso_id')->references('id')->on('aplicacion_uso_utilitarios')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('fecha_venta_id')->references('id')->on('fecha_ventas')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('modelo_id')->references('id')->on('modelo_utilitarios')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('tipo_id')->references('id')->on('tipo_utilitarios')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('traccion_id')->references('id')->on('traccion_utilitarios')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('peso_id')->references('id')->on('peso_utilitarios')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('capacidad_carga_id')->references('id')->on('capacidad_carga_utilitarios')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('cilindrada_id')->references('id')->on('cilindradas')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('potencia_id')->references('id')->on('potencia_utilitarios')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('direccion_id')->references('id')->on('direcciones')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('kilometraje_id')->references('id')->on('kilometrajes')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('equipamiento_id')->references('id')->on('equipamiento_utilitarios')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('estado_id')->references('id')->on('estados')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('altura_cabina_id')->references('id')->on('altura_cabinas')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('color_cabina_id')->references('id')->on('color_cabinas')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('desgaste_neumatico_id')->references('id')->on('desgaste_neumaticos')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('camiones_equipamiento', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('camion_id')->unsigned()->nullable();
            $table->integer('equipamiento_camion_id')->unsigned()->nullable();

            // Foreigns
            $table->foreign('camion_id')->references('id')->on('camiones')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('equipamiento_camion_id')->references('id')->on('equipamiento_camiones')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });

        

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('utilitarios');
        Schema::drop('buses');
        Schema::drop('camiones');
        Schema::drop('fecha_ventas');
        Schema::drop('desgaste_neumaticos');
        Schema::drop('transmisiones');
        Schema::drop('configuracion_carroceria_buses');
        Schema::drop('marca_carroceria_buses');
        Schema::drop('tipo_carrocerias');
        Schema::drop('color_cabinas');
        Schema::drop('altura_cabinas');
        Schema::drop('estados');
        Schema::drop('tanque_adicional_camiones');
        Schema::drop('equipamiento_utilitarios');
        Schema::drop('equipamiento_buses');
        Schema::drop('equipamiento_camiones');
        Schema::drop('kilometrajes');
        Schema::drop('direcciones');
        Schema::drop('potencia_utilitarios');
        Schema::drop('potencias');
        Schema::drop('cilindradas');
        Schema::drop('capacidad_carga_utilitarios');
        Schema::drop('capacidad_carga_camiones');
        Schema::drop('peso_utilitarios');
        Schema::drop('peso_camiones');
        Schema::drop('traccion_utilitarios');
        Schema::drop('traccion_buses');
        Schema::drop('traccion_camiones');
        Schema::drop('tipo_utilitarios');
        Schema::drop('tipo_buses');
        Schema::drop('tipo_camiones');
        Schema::drop('modelo_utilitarios');
        Schema::drop('modelo_buses');
        Schema::drop('modelo_camiones');
        Schema::drop('marcas');
        Schema::drop('aplicacion_uso_utilitarios');
        Schema::drop('aplicacion_uso_buses');
        Schema::drop('aplicacion_uso_camiones');
    }
}
