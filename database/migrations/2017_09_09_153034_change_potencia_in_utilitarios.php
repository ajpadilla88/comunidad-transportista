<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePotenciaInUtilitarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('utilitarios', function(Blueprint $table) {
            $table->dropForeign('utilitarios_potencia_id_foreign');
            $table->foreign('potencia_id')->references('id')->on('potencias')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::drop('potencia_utilitarios');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('utilitarios', function(Blueprint $table) {
            $table->dropForeign('utilitarios_potencia_id_foreign');
            $table->foreign('potencia_id')->references('id')->on('potencia_utilitarios')->onUpdate('cascade')->onDelete('cascade');
        });
    }
}
