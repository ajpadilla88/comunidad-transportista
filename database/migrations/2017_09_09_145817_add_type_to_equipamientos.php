<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeToEquipamientos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('equipamiento_camiones', function (Blueprint $table) {
            $table->string('type')->after('id');
        });
        Schema::table('equipamiento_buses', function (Blueprint $table) {
            $table->string('type')->after('id');
        });
        Schema::table('equipamiento_utilitarios', function (Blueprint $table) {
            $table->string('type')->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('equipamiento_camiones', function (Blueprint $table) {
            $table->dropColumn('type');
        });
        Schema::table('equipamiento_buses', function (Blueprint $table) {
            $table->dropColumn('type');
        });
        Schema::table('equipamiento_utilitarios', function (Blueprint $table) {
            $table->dropColumn('type');
        });
    }
}
