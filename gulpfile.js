const elixir = require('laravel-elixir');

require('laravel-elixir-vue');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    /* Application SASS */
    mix.sass(['app.scss']);

    /* Stylesheets */
    mix.styles([
        //'bower_components/bootstrap/dist/css/bootstrap.min.css',
        'bower_components/font-awesome/css/font-awesome.min.css',
    ], 'public/css/base.css', './');

    mix.styles([
        'bower_components/bootstrap/dist/css/bootstrap.min.css',
        'bower_components/font-awesome/css/font-awesome.min.css',
        'resources/assets/packages/inspinia/css/animate.css',
        'bower_components/select2/dist/css/select2.min.css',
        //'bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css',
        'bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
        'bower_components/summernote/dist/summernote.css',
        'bower_components/pace/themes/pace-theme-flat-top.css',
    ], 'public/css/admin.css', './');

    mix.less([
        './resources/assets/packages/inspinia/less/style.less',
        './resources/assets/packages/inspinia/custom.less'
    ], 'public/css/admin-theme.css', './');

    /* Scripts */
    mix.scripts([
        'bower_components/jquery/dist/jquery.min.js',
        'bower_components/bootstrap/dist/js/bootstrap.min.js',
        'bower_components/masonry/dist/masonry.pkgd.min.js'
    ], 'public/js/bottom.js', './');

    mix.scripts([
        'bower_components/jquery/dist/jquery.min.js',
        'bower_components/bootstrap/dist/js/bootstrap.min.js',
        'bower_components/metisMenu/dist/metisMenu.min.js',
        'bower_components/jquery-slimscroll/jquery.slimscroll.js',
        //'bower_components/bootstrap-datepicker/js/bootstrap-datepicker.js',
        'bower_components/moment/min/moment.min.js',
        'bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
        'bower_components/select2/dist/js/select2.js',
        'bower_components/spin.js/spin.js',
        'bower_components/spin.js/jquery.spin.js',
        'resources/assets/packages/formvalidation/dist/js/formValidation.js',
        'resources/assets/packages/formvalidation/dist/js/framework/bootstrap.js',
        'bower_components/summernote/dist/summernote.min.js',
        'resources/assets/packages/inspinia/js/inspinia.js'
    ], 'public/js/admin.js', './');

    /* Fonts */
    mix.copy([
        'bower_components/font-awesome/fonts',
        'bower_components/bootstrap/dist/fonts',
        'resources/assets/fonts'
    ], 'public/build/fonts');

    mix.copy([
        'bower_components/summernote/dist/font',
    ], 'public/build/css/font');

    /* Gallery images */
    //mix.copy([
    //    'resources/assets/packages/blueimp/img'
    //], 'public/build/img');

    mix.version(['css/base.css', 'css/app.css', 'css/admin.css', 'css/admin-theme.css', 'js/bottom.js', 'js/admin.js']);

    //mix.sass('app.scss')
    //   .webpack('app.js');
});
