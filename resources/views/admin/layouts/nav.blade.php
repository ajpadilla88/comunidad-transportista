<nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
        {{--<a class="hidden-xs" style="display: inline-table; float: left;line-height: 55px; margin-left: 10px;">--}}
            {{--<img class="" src="/img/logo.png" height="30">--}}
        {{--</a>--}}
    </div>
    <ul class="nav navbar-top-links navbar-right pull-right">
        @if ($messages)
            <li class="dropdown">
                <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#" aria-expanded="false">
                    <i class="fa fa-fw fa-envelope-o"></i>  <span class="label label-warning">{!! $messages->count() !!}</span>
                </a>
                <ul class="dropdown-menu dropdown-messages">
                    @foreach ($messages as $message)
                        <li class="">
                            <div class="dropdown-messages-box">
                                <div class="media-body">
                                    {!! $message->body !!}
                                    <small class="text-muted block">{!! $message->date_human !!} by {!! $message->author !!}</small>
                                </div>
                            </div>
                        </li>
                    @endforeach
                    <li class="no-padding">
                        <div class="no-margins text-right">
                            <a href="{!! route('admin.messages.index') !!}">
                                <strong>Read All</strong>
                            </a>
                        </div>
                    </li>
                </ul>
            </li>
        @endif

        @if ($notifications)
            <li class="dropdown">
                <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#" aria-expanded="false">
                    <i class="fa fa-fw fa-globe"></i>  <span class="label label-warning">{!! $notifications->count() !!}</span>
                </a>
                <ul class="dropdown-menu dropdown-messages">
                    @foreach ($notifications as $notification)
                        <li class="">
                            <div class="dropdown-notifications-box">
                                <div class="media-body">
                                    {!! $notification->body !!}
                                    <small class="text-muted block">{!! $notification->date_human !!} by {!! $notification->author !!}</small>
                                </div>
                            </div>
                        </li>
                    @endforeach
                    <li class="no-padding">
                        <div class="no-margins text-right">
                            <a href="{!! route('admin.notifications.index') !!}">
                                <strong>Read All</strong>
                            </a>
                        </div>
                    </li>
                </ul>
            </li>
        @endif

        <li>
            <a href="{{ url('auth/logout') }}" class="">
                Log out
                <i class="fa fa-power-off fa-fw m-l-xs"></i> <span class="hidden-xs"> {!! $first_name !!} </span>
            </a>
        </li>
    </ul>
</nav>

