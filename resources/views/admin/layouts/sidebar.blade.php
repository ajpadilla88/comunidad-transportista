<ul class="nav" id="side-menu">
    <li class="nav-header border-bottom">
        <div class="dropdown profile-element">
            <a href="{!! route('admin.home') !!}">
                <img alt="image" class="" src="/img/logo.jpg" height="38">
            </a>
            {{--<img src="/img/white-isologo.png" height="38">--}}
        </div>
        <div class="logo-element">
            <a href="{!! route('admin.home') !!}">
                <img src="/img/logo.jpg" height="50">
            </a>
        </div>
    </li>
    @foreach ($primary_menu as $menu)
        <li class="{{ (isset($menu['active']) and $menu['active']) ? 'active' : '' }}">
            @if (!isset($menu['items']))
                <a href="{{ $menu['link'] }}" {{ isset($menu['target']) && $menu['target'] ? ' target="' . $menu['target']. '"' : '' }}>
                    @if (isset($menu['icon']))<i class="fa fa-{{ $menu['icon'] }}"></i>@endif
                    <span class="nav-label">{{ $menu['title'] }}</span>
                </a>
            @else
                <a href="#">
                    <i class="fa fa-{{ $menu['icon'] }}"></i>
                    <span class="nav-label">{{$menu['title']}}</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse">
                    @foreach ($menu['items'] as $item)
                        <li>
                            <a href="{{ $item['link'] }}">
                                @if (isset($item['icon']))<i class="fa fa-{{$item['icon']}} fa-fw"></i>@endif
                                {{$item['title']}}
                            </a>
                        </li>
                    @endforeach
                </ul>
            @endif
        </li>
    @endforeach
</ul>