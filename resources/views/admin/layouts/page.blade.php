<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="csrf-param" content="_token">

        <title> @yield('page_title') | Alianza Arboles </title>

        <link rel="stylesheet" href="{{ elixir('css/admin.css') }}">
        <link rel="stylesheet" href="{{ elixir('css/admin-theme.css') }}">
    </head>
    <body class="skin-3 mini-navbar">
        <div id="wrapper">
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    @include('admin.layouts.sidebar')
                </div>
            </nav>

            <div id="page-wrapper" class="gray-bg">
                <div class="row border-bottom">
                    @include('admin.layouts.nav')
                </div>
                @if (view()->hasSection('breadcrumbs'))
                <div class="row wrapper border-bottom white-bg page-heading">
                    <div class="col-lg-12">
                        <h2>@yield('page_title')</h2>
                        @yield('breadcrumbs')
                    </div>
                </div>
                @endif
                <div class="wrapper wrapper-content animated fadeInUp">
                    @include('admin.layouts.alerts')
                    @yield('content')
                </div>
                <div class="footer">
                    <div>
                        <strong>Copyright</strong> PictureKeeper &copy; {{ date('Y') }}
                    </div>
                </div>
            </div>
        </div>

        <script  type="text/javascript" src="{{ elixir('js/admin.js') }}"></script>

        @yield('inline_scripts')
    </body>
</html>
