<table class="table table-hover table-stripped table-condensed packages-table">
    <thead>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Creator</th>
        <th>Date</th>
        <th>Location</th>
        <th class="text-right">Actions</th>
    </tr>
    </thead>
    <tbody>
        @forelse($items as $item)
        <tr>
            <td>{!! $item->id !!}</td>
            <td>{!! $item->name !!}</td>
            <td>{!! $item->creator_name !!}</td>
            <td>{!! $item->date_friendly !!}</td>
            <td>{!! $item->location!!}</td>
            <td class="text-right">
                <a href="{!! route('admin.activities.edit', $item->id) !!}" class="btn btn-sm btn-default">Edit</a>
            </td>
        </tr>
        @empty
        <tr>
            <td colspan="6">No records found.</td>
        </tr>
        @endforelse
    </tbody>
</table>

<style>
    .packages-table.table td,  .packages-table.table > thead >tr > th {
        padding: 5px 5px;
    }
    .packages-table td .text-inline { display: inline-flex;}
</style>