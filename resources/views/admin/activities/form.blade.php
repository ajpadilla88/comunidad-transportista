<div class="ibox-title">
    <h3>Settings</h3>
</div>
<div class="ibox-content">
    <div class="form-group">
        {!! Form::label('created_by', 'Created By') !!}
        {!! Form::select('created_by', $users->pluck('email', 'id'), null, ['class'=>'form-control', 'placeholder'=>'Select...', 'style' => 'width: 100%;']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('name', 'Name') !!}
        {!! Form::input('text', 'name', null, ['class'=>'form-control', 'placeholder'=>'Enter...']) !!}
    </div>

    <div class="form-group" id="picker" style="position: relative;">
        {!! Form::label('date', 'Date') !!}
        {!! Form::input('text', 'date', isset($activity->date) ? Carbon\Carbon::parse($activity->date)->format('d/m/Y H:i') : null, ['class'=>'form-control datepicker', 'placeholder'=>'Enter...']) !!}
        <p class="help-block"><small>Format: dd/mm/yyyy hh:mm</small></p>
    </div>

    <div class="form-group">
        {!! Form::label('location', 'Location') !!}
        {!! Form::input('text', 'location', null, ['class'=>'form-control', 'placeholder'=>'Enter...', 'id' => 'location', 'onFocus' => "geolocate()"]) !!}
    </div>

    <div class="hr-line-dashed"></div>

    <h2>
        Actions
        <a class="m-l-xs select-all" href="#" style="font-size: 10px;">Select all</a>
        <a class="m-l-xs deselect-all" href="#" style="font-size: 10px;">Deselect all</a>
    </h2>

    <div class="form-group">
        @foreach ($actions as $action)
            <div class="checkbox m-b">
                <label>
                    <input type="checkbox" name="actions[{!! $action->id !!}]" value="1" {!! $activity->containsAction($action->getWrappedObject()) ? 'checked' : '' !!}> {!! $action->name !!}
                    <input class="input-xs text-center m-l-sm" type="text" name="actions[{!! $action->id !!}][amount]" value="0">
                </label>
            </div>
        @endforeach
    </div>

    <div class="hr-line-dashed"></div>

    <div class="form-group">
        {!! Form::label('teaser', 'Teaser / Short Summary') !!}
        {!! Form::textarea('teaser', null, ['id' => 'teaser', 'class' => 'form-control', 'placeholder' => 'Enter...']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('body', 'Body / Full description') !!}
        {!! Form::textarea('body', null, ['id' => 'body', 'class' => 'form-control', 'placeholder' => 'Enter...']) !!}
    </div>


    <div class="hr-line-dashed"></div>
    {!! Form::submit(null, ['class'=> 'btn btn-primary']) !!}
</div>

@section('inline_scripts')

    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={!! env('GOOGLE_PLACES_API_KEY') !!}&libraries=places"></script>

    <script type="text/javascript">
        var input = document.getElementById('location');
        autocomplete = new google.maps.places.Autocomplete(input, {});

        // Bias the autocomplete object to the user's geographical location,
        // as supplied by the browser's 'navigator.geolocation' object.
        function geolocate() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    var geolocation = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    var circle = new google.maps.Circle({
                        center: geolocation,
                        radius: position.coords.accuracy
                    });
                    autocomplete.setBounds(circle.getBounds());
                });
            }
        }

        $(document).ready(function () {
            var $form = $('.ibox form');
            $form
                    .find('select').select2().end()
                    .find('.datepicker').datetimepicker({
                        format: 'DD/MM/Y HH:mm',
                        sideBySide: true
                    }).end()
                    .find('#teaser').summernote({ height: 200 }).end()
                    .find('#body').summernote({ height: 500 }).end()
                    .formValidation({
                        framework: 'bootstrap',
                        icon: {
                            invalid: 'glyphicon glyphicon-remove',
                            validating: 'glyphicon glyphicon-refresh'
                        },
                        fields: {
                            'name': {
                                validators: {
                                    notEmpty: {},
                                    stringLength: {min: 3, max: 255}
                                }
                            },
                            'created_by': {
                                validators: {
                                    notEmpty: {}
                                }
                            },
                            'location': {
                                validators: {
                                    notEmpty: {}
                                }
                            }
                        },
                        excluded: ':disabled'
                    });
        });
    </script>
@endsection