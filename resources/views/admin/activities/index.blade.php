@extends('admin.layouts.page')

@section('page_title') Activities @stop

@section('breadcrumbs')
    <div class="header-actions">
        {!! link_to_route('admin.activities.create', 'Create Activity', null, ['class'=>'btn btn-primary btn-outline btn-flat btn-md pull-right']) !!}
    </div>
    <ol class="breadcrumb">
        <li><strong>Home</strong></li>
    </ol>
@stop

@section('content')

    <div class="i-box">
        <div class="ibox-content">

            <div class="table-responsive m-t">
                {!! $table !!}
            </div>

            {!! $items->appends($params)->render() !!}
        </div>
    </div>

@stop