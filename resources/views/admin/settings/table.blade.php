<table class="table table-hover table-stripped table-condensed">
    <thead>
    <tr>
        <th>Id</th>
        <th>Key</th>
        <th>Value</th>
        <th class="text-right">Actions</th>
    </tr>
    </thead>
    <tbody>
        @forelse($items as $item)
        <tr>
            <td>{!! $item->id !!}</td>
            <td>{!! $item->key !!}</td>
            <td>{!! $item->value !!}</td>
            <td class="text-right">
                <div class="btn-group">
                    <a class="btn btn-default btn-flat btn-sm" href="{{ route('admin.settings.edit', $item->id) }}"><i class="fa fa-fw fa-pencil"></i>Edit</a>
                </div>
            </td>
        </tr>
        @empty
        <tr>
            <td colspan="4">No records found.</td>
        </tr>
        @endforelse
    </tbody>
</table>