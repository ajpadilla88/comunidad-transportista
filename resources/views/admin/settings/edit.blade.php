@extends('admin.layouts.page')
@section('page_title') Edit Setting @stop
@section('content')

    <div class="i-box">
        <div class="ibox-title">
            <h2>Edit Setting</h2>
        </div>
        <div class="ibox-content">
            {!! Form::model($setting, ['route' => ['admin.settings.update', $setting->id], 'method' => 'PUT']) !!}
            @include('admin.settings.form')
            {!! Form::close() !!}
        </div>
    </div>
@stop