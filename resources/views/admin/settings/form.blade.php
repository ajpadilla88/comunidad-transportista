<div class="form-group">
    {!! Form::label('key', 'Key') !!}
    {!! Form::input('text', 'key', null, ['class'=>'form-control', 'placeholder'=>'Enter...', 'data-bv-notempty'=>'true']) !!}
</div>

<div class="form-group">
    {!! Form::label('value', 'Value') !!}
    {!! Form::textarea('value', null, ['class'=>'form-control', 'placeholder'=>'Enter...', 'data-bv-notempty'=>'true']) !!}
</div>

<div class="hr-line-dashed"></div>

{!! Form::submit('Save', ['class'=> 'btn btn-primary btn-lg']) !!}


<style>
    .form-group label {
        font-size: 16px;
        font-weight: 300;
    }
</style>

@section('inline_scripts')
    <script>
        jQuery(function ($) {
            $('.ibox-content form').bootstrapValidator({
                excluded: ':disabled'
            });
        });
    </script>
@append