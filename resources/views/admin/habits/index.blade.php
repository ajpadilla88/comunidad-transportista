@extends('admin.layouts.page')

@section('page_title') Habits @stop

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><strong>Home</strong></li>
    </ol>
@stop

@section('content')

    <div class="i-box">
        <div class="ibox-content">

            <div class="table-responsive m-t">
                {!! $table !!}
            </div>

            {!! $items->appends($params)->render() !!}
        </div>
    </div>

@stop