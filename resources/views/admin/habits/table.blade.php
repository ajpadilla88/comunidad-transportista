<table class="table table-hover table-stripped table-condensed packages-table">
    <thead>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Type</th>
        <th class="text-center">Users</th>
    </tr>
    </thead>
    <tbody>
        @forelse($items as $item)
        <tr>
            <td>{!! $item->id !!}</td>
            <td>{!! $item->name !!}</td>
            <td>{!! $item->type !!}</td>
            <td class="text-center">{!! $item->user_count !!}</td>
            <td class="text-right"></td>
        </tr>
        @empty
        <tr>
            <td colspan="5">No records found.</td>
        </tr>
        @endforelse
    </tbody>
</table>

<style>
    .packages-table.table td,  .packages-table.table > thead >tr > th {
        padding: 5px 5px;
    }
    .packages-table td .text-inline { display: inline-flex;}
</style>