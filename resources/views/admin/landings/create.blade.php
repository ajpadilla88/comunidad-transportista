@extends('admin.layouts.page')
@section('page_title') Create Landing @stop
@section('content')

    <div class="i-box">
        <div class="ibox-title">
            <h2>Create Landing</h2>
        </div>
        <div class="ibox-content">
            {!! Form::model($landing, ['route' => 'admin.landings.store', 'method' => 'POST', 'files' => true,
                'data-bv-feedbackicons-valid' => 'glyphicon glyphicon-ok',
                'data-bv-feedbackicons-invalid' => 'glyphicon glyphicon-remove',
                'data-bv-feedbackicons-validating' => 'glyphicon glyphicon-refresh'
            ]) !!}
            @include('admin.landings.form')
            {!! Form::close() !!}
        </div>
    </div>
@stop