<table class="table table-hover table-stripped table-condensed packages-table">
    <thead>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Url / Slug</th>
        <th>Section</th>
        <th>Created At</th>
        <th>Updated At</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
        @forelse($items as $item)
        <tr>
            <td>{!! $item->id !!}</td>
            <td>{!! $item->name !!}</td>
            <td>{!! $item->slug !!}</td>
            <td>{!! $item->section !!}</td>
            <td>{!! $item->created_at !!}</td>
            <td>{!! $item->updated_at !!}</td>
            <td class="text-right">
                <div class="btn-group">
                    <a class="btn btn-default btn-flat btn-sm" href="{{ url($item->getUrlSlug()) }}" target="_blank">Preview</a>
                    <a class="btn btn-default btn-flat btn-sm" href="{{ route('admin.landings.edit', $item->id) }}"><i class="fa fa-fw fa-pencil"></i>Edit</a>
                    <button type="button" class="btn btn-default btn-flat dropdown-toggle btn-sm bg-white" data-toggle="dropdown">
                        <i class="fa fa-fw fa-cog"></i> <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>

                    <ul class="dropdown-menu dropdown-menu-right" role="menu">
                        <li><a href="{{ route('admin.landings.destroy', $item->id) }}" data-confirm="Are you sure?" data-method="DELETE"><i class="fa fa-fw fa-trash-o"></i>Delete</a></li>
                    </ul>
                </div>
            </td>
        </tr>
        @empty
        <tr>
            <td colspan="7">No records found.</td>
        </tr>
        @endforelse
    </tbody>
</table>

<style>
    .packages-table.table td,  .packages-table.table > thead >tr > th {
        padding: 5px 5px;
    }
    .packages-table td .text-inline { display: inline-flex;}
</style>