<div class="form-group">
    {!! Form::label('name', 'Name') !!}
    {!! Form::input('text', 'name', null, ['class'=>'form-control', 'placeholder'=>'Enter...', 'data-bv-notempty'=>'true']) !!}
</div>

<div class="form-group">
    {!! Form::label('slug', 'Slug / URL') !!}
    {!! Form::input('text', 'slug', null, ['class'=>'form-control', 'placeholder'=>'Enter...', 'data-bv-notempty'=>'true']) !!}
</div>

<div class="form-group">
    {!! Form::label('section', 'Section') !!}
    {!! Form::select('section', ['frontend' => 'Front End', 'backend' => 'Back End'], null, ['class'=>'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('icon', 'Icon') !!}
    {!! Form::input('text', 'icon', null, ['class'=>'form-control', 'placeholder'=>'Enter...']) !!}
</div>

<div class="form-group m-t-lg">
    <h3>Custom Html</h3>
    {!! Form::textarea('html', null, ['id' => 'html_code', 'class' => 'form-control', 'placeholder' => 'Enter...']) !!}
</div>

<div class="form-group m-t-lg">
    <h3>Custom Head</h3>
    {!! Form::textarea('head', null, ['id' => 'head_code', 'class' => 'form-control', 'placeholder' => 'Enter...']) !!}
    <p class="m-t">
            <u>Example:</u>
<code>&lt;meta name="description" content="Free Web tutorials"&gt;
&lt;meta name="keywords" content="HTML,CSS,XML,JavaScript"&gt;
&lt;meta name="author" content="Hege Refsnes"&gt;</code>
    </p>
</div>

<div class="form-group m-t-lg">
    <h3>Custom CSS</h3>
    {!! Form::textarea('styles', null, ['id' => 'css_code', 'class' => 'form-control', 'placeholder' => 'Enter...']) !!}
    <p class="m-t">
        <u>Example:</u>
<code>&lt;style&gt;
body {
    font-size: 12px;
}
&lt;/style&gt;</code>
    </p>
</div>

<div class="form-group m-t-lg">
    <h3>Custom JavaScript</h3>
    {!! Form::textarea('scripts', null, ['id' => 'js_code', 'class' => 'form-control', 'placeholder' => 'Enter...']) !!}
    <p class="m-t">
        <u>Example:</u>
<code>&lt;script&gt;
jQuery(function($) {

});
&lt;/script&gt;</code>
    </p>
</div>

<div class="hr-line-dashed"></div>

{!! Form::submit('Save', ['class'=> 'btn btn-primary btn-lg']) !!}


<style>
    .note-editor.note-frame {
        border-color: #ddd;
    }
    form code {
        white-space: pre-wrap;
        display: block;
        color: inherit;
        background: #ffffe1;
        padding: 10px;
        margin: 10px 0;
    }

    .form-group label {
        font-size: 16px;
        font-weight: 300;
    }

    .CodeMirror-scroll {
        min-height: 100px;
    }
</style>

@section('inline_scripts')
    <script>
        jQuery(function ($) {
            $('.ibox-content form').bootstrapValidator({
                excluded: ':disabled'
            });

            $('#html_code').summernote({height: 500});

            var editor2 = CodeMirror.fromTextArea(document.getElementById("head_code"), {lineNumbers: true});
            var editor3 = CodeMirror.fromTextArea(document.getElementById("css_code"), {lineNumbers: true});
            var editor4 = CodeMirror.fromTextArea(document.getElementById("js_code"), {
                lineNumbers: true,
                mode: "javascript"
            });
        });
    </script>
@append