@extends('admin.layouts.page')
@section('page_title') Edit Landing @stop
@section('content')

    <div class="i-box">
        <div class="ibox-title">
            <h2>Edit Landing</h2>
        </div>
        <div class="ibox-content">
            {!! Form::model($landing, ['route' => ['admin.landings.update', $landing->id], 'method' => 'PUT', 'files' => true,
                'data-bv-feedbackicons-valid' => 'glyphicon glyphicon-ok',
                'data-bv-feedbackicons-invalid' => 'glyphicon glyphicon-remove',
                'data-bv-feedbackicons-validating' => 'glyphicon glyphicon-refresh'
            ]) !!}
            @include('admin.landings.form', compact('landing'))
            {!! Form::close() !!}
        </div>
    </div>
@stop