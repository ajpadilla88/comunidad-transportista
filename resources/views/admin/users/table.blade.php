<table class="table table-hover table-stripped table-condensed packages-table">
    <thead>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>E-mail</th>
        <th class="text-center">Donor?</th>
        <th>Location</th>
        <th class="text-center">Admin?</th>
        <th class="text-center">Created At</th>
        <th class="text-center">Updated At</th>
    </tr>
    </thead>
    <tbody>
        @forelse($items as $item)
        <tr>
            <td>{!! $item->id !!}</td>
            <td>{!! $item->name !!}</td>
            <td>{!! $item->email !!}</td>
            <td class="text-center">{!! $item->donor ? 'Yes' : 'No' !!}</td>
            <td>{!! $item->location !!}</td>
            <td class="text-center">{!! $item->super_admin ? 'Yes' : 'No' !!}</td>
            <td class="text-center">{!! $item->created_at !!}</td>
            <td class="text-center">{!! $item->updated_at !!}</td>
        </tr>
        @empty
        <tr>
            <td colspan="8">No records found.</td>
        </tr>
        @endforelse
    </tbody>
</table>

<style>
    .packages-table.table td,  .packages-table.table > thead >tr > th {
        padding: 5px 5px;
    }
    .packages-table td .text-inline { display: inline-flex;}
</style>