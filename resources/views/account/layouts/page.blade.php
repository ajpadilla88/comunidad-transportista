<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="csrf-param" content="_token">

        <title> @yield('page_title') | Alianza Arboles </title>

        <link rel="stylesheet" href="{{ elixir('css/admin.css') }}">
        <link rel="stylesheet" href="{{ elixir('css/admin-theme.css') }}">
        @yield('css')
    </head>
    <body >
        <div id="wrapper">
            <div class="row">
                <div class="col-lg-12" style="position: fixed;
    z-index: 200; box-shadow: 0px 3px 1px rgba(0, 0, 0, 0.25)">
                    @include('account.layouts.nav')
                </div>
            </div>
            <div class="row" style="margin-top: 58px">
                
            <nav class="navbar navbar-static-side" role="navigation" >
                <div >
                    @include('account.layouts.sidebar')
                </div>
            </nav>


            <div id="page-wrapper" class="gray-bg">
                
                @if (view()->hasSection('breadcrumbs'))
                <div class="row wrapper border-bottom white-bg page-heading">
                    <div class="col-lg-12">
                        <h2>@yield('page_title')</h2>
                        @yield('breadcrumbs')
                    </div>
                </div>
                @endif
                <div class="wrapper wrapper-content animated fadeInUp">
                    @include('account.layouts.alerts')
                    @yield('content')
                </div>
                <div class="footer">
                    <div>
                        <strong>Copyright</strong> PictureKeeper &copy; {{ date('Y') }}
                    </div>
                </div>
            </div> 
            </div>
        </div>
        <script  type="text/javascript" src="{{ elixir('js/admin.js') }}"></script>

        @yield('inline_scripts')
    </body>
</html>
