<ul class="nav" >
    <li class="nav-header ">

        

        
        <div class="logo-element">
            <a href="{!! route('account.home') !!}">
                
                Hola, {!! $first_name !!}
            </a>
        </div>
    </li>
    @foreach ($primary_menu as $menu)
        <li class="{{ (isset($menu['active']) and $menu['active']) ? 'active' : '' }}">
            @if (!isset($menu['items']))
                <a href="{{ $menu['link'] }}" {{ isset($menu['target']) && $menu['target'] ? ' target="' . $menu['target']. '"' : '' }}>
                    @if (isset($menu['icon']))<i class="fa fa-{{ $menu['icon'] }}"></i>@endif
                    <span class="">{{ $menu['title'] }}</span>
                </a>
            @else
                <a href="#">
                    <i class="fa fa-{{ $menu['icon'] }}"></i>
                    <span class="">{{$menu['title']}}</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse">
                    @foreach ($menu['items'] as $item)
                        <li>
                            <a href="{{ $item['link'] }}">
                                @if (isset($item['icon']))<i class="fa fa-{{$item['icon']}} fa-fw"></i>@endif
                                {{$item['title']}}
                            </a>
                        </li>
                    @endforeach
                </ul>
            @endif
        </li>
    @endforeach
</ul>