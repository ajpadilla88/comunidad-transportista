@if (!empty($alerts))
    <div class="alerts text-left">
        <!-- Validation Errors -->
        @foreach($alerts as $type => $messages)
            <div class="alert alert-{{$type}} alert-dismissable">
                @if (is_array($messages))
                    <ul>
                        @foreach($messages as $m)
                            <li>{{ $m }}</li>
                        @endforeach
                    </ul>
                @else
                    {{ $messages }}
                @endif
            </div>
        @endforeach
    </div>
@endif

@if(Session::has('message_success'))
    <div class="alert alert-success">
        <span class="glyphicon glyphicon-ok"></span>
        <em> {!! session('message_success') !!}</em>
    </div>
@endif