<nav class="navbar navbar-static-top " role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        
        <a class="hidden-xs" style="display: inline-table; float: left;line-height: 55px; margin-left: 10px;">
            <img class="" src="/img/logo.png" height="50">
        </a>
    </div>
    <ul class="nav navbar-top-links navbar-right pull-right">
        <li>
            <button type="button" class="btn btn-primary btn-lg"><a href="{{ url('account/vehicles/') }}" >Publicar</a></button>

        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{!! $first_name !!} <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="{{ route('account.user.profile.show') }}">Perfil</a></li>
            <li role="separator" class="divider"></li>
            <li>
                <a href="{{ url('auth/logout') }}" class="">
                Log out
                <i class="fa fa-power-off fa-fw m-l-xs"></i> <span class="hidden-xs"> </span>
            </a>
            </li>
          </ul>
        </li>
    </ul>
</nav>

