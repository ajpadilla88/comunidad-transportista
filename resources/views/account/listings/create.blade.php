@extends('admin.layouts.page')

@section('page_title') Create Activity @stop

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{!! route('root') !!}">Home</a></li>
        <li>{!! link_to_route('admin.activities.index', 'Activities') !!}</li>
        <li><b>Create</b></li>
    </ol>
@stop

@section('content')
    <div class="ibox">
        {!! Form::model($activity, ['route' => 'admin.activities.store', 'method' => 'POST', 'files' => true]) !!}
        @include('admin.activities.form')
        {!! Form::close() !!}
    </div>
@stop