@extends('account.layouts.page')

@section('page_title') Create Vehicle @stop

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{!! route('root') !!}">Home</a></li>
        <li>{!! link_to_route('account.vehicles.index', 'Vehicles') !!}</li>
        <li><b>Create</b></li>
    </ol>
@stop

@section('css')
    <link href="{{ asset('/css/dropzone.css') }}" rel="stylesheet">
@stop

@section('content')
    <div class="ibox">
        {!! Form::open(['route' => 'account.vehicles.store', 'method' => 'POST', 'files' => true]) !!}
        @include('account.vehicles.form')
        {!! Form::close() !!}
    </div>
@stop