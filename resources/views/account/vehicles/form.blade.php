
    <div class="ibox fve">
        <div class="ibox-title">
            <h3>Detalles del Vehículo</h3>
        </div>
<div class="ibox-content">
    <div class="row">
        <div class="form-group col-lg-3">
            {!! Form::label('type', 'Vehículo') !!}
            {!! Form::select('type', ['1' => 'Bus', '2' => 'Camion', '3' => 'Utilitario'], null, ['class'=>'form-control', 'placeholder'=>'Select...', 'style' => 'width: 100%;', 'id' => 'typeVehicle', 'onChange' => 'loadMarcas()']) !!}
        </div>

        <div class="form-group col-lg-3">
            {!! Form::label('marca', 'Marca') !!}

            {!! Form::select('marca', $marcas->pluck('name', 'id'), null, ['id'=>'marca', 'class'=>'form-control', 'placeholder'=>'Select...', 'style' => 'width: 100%;', 'onChange' => 'loadModelos()']) !!}        
        </div>

        <div class="form-group col-lg-3">
            {!! Form::label('modelo_camion_id', 'Modelo') !!}

            {!! Form::select('modelo_camion_id', [], null, ['id' => 'modelo', 'class'=>'form-control', 'placeholder'=>'Select...', 'style' => 'width: 100%;', 'onChange' => 'loadAppUso()']) !!}  
        </div>

        <div class="form-group  col-lg-3">
            {!! Form::label('aplicacion_uso_id', 'Aplicación de Uso') !!}
            {!! Form::select('aplicacion_uso_id', [], null, ['id' => 'aplicacion','class'=>'form-control', 'placeholder'=>'Select...', 'style' => 'width: 100%;']) !!}
        </div>

    
        <div class="form-group  col-lg-3">
            {!! Form::label('condicion', 'Condicion') !!}
            {!! Form::select('condicion', ['1' => 'Nuevo', '2' => 'Usado'], null, ['id' => 'condicion', 'class'=>'form-control', 'placeholder'=>'Select...', 'style' => 'width: 100%;', 'onChange' => 'loadYear()']) !!}
        </div>

        <div class="form-group col-lg-3" id="picker" style="position: relative;">
            {!! Form::label('anio', 'Date') !!}
            {!! Form::input('text', 'anio', isset($activity->date) ? Carbon\Carbon::parse($activity->date)->format('Y') : null, ['class'=>'form-control datepicker', 'placeholder'=>'Enter...']) !!}
            <p class="help-block"><small>Format: yyyy</small></p>
        </div>
         <div class="hr-line-dashed  col-lg-12"></div>
        <div class="form-group  col-lg-3">
            {!! Form::label('state_id', 'Provincia') !!}
            {!! Form::select('state_id', $state->pluck('name', 'id'),null, ['class'=>'form-control', 'placeholder'=>'Enter...']) !!}
        </div>

        <div class="form-group  col-lg-3">
            {!! Form::label('location', 'Ubicación fisica') !!}
            {!! Form::input('text', 'location', null, ['class'=>'form-control', 'placeholder'=>'Enter...', 'id' => 'location', 'onFocus' => "geolocate()"]) !!}
        </div>

        <div class="form-group  col-lg-3">
            {!! Form::label('kilometraje_id', 'KM') !!}
            {!! Form::select('kilometraje_id', $kms->pluck('name', 'id'), null, ['class'=>'form-control', 'placeholder'=>'Select...', 'style' => 'width: 100%;']) !!}
        </div>
        <div class="hr-line-dashed  col-lg-12"></div>
        <div class="form-group  col-lg-3">
            {!! Form::label('price', 'Precio de venta') !!}
            {!! Form::input('text', 'price', null, ['class'=>'form-control', 'placeholder'=>'Enter...']) !!}
        </div>

        <div class="form-group  col-lg-3">
            {!! Form::label('fecha_venta_id', 'Fecha de Entrega') !!}
            {!! Form::select('fecha_venta_id', $fechaVenta->pluck('name', 'id'), null, ['id'=>'fechav','class'=>'form-control', 'placeholder'=>'Select...', 'style' => 'width: 100%;']) !!}
        </div>

        <div class="form-group  col-lg-6">
            {!! Form::label('pago', 'Forma de Pago') !!}
            {!! Form::select('pago', ['1' => 'Contado', '2' => 'Permuto', '3' => 'Financiado', '4' => 'Tomo Usado'], null, ['class'=>'form-control ff','multiple' => 'multiple',  'style' => 'width: 100%;']) !!}
            
        </div>
    </div>
    {!! Form::button('Siguiente', ['class'=> 'btn btn-primary', 'onClick' => 'nextSheet(2)']) !!}
        
    </div>
</div>

<div class="ibox ftecnicacam" >
    <div class="ibox-title">
        <h3>Detalles de publicación</h3>
    </div>
    <div class="ibox-content">
        <div class="row">
            <div class="form-group  col-lg-12">
                {!! Form::label('title', 'Titulo') !!}
                {!! Form::input('text', 'title', null, ['class'=>'form-control', 'placeholder'=>'Enter...', 'id' => 'title']) !!}
            </div>


            <div class="form-group  col-lg-12">
          
                <div id="my-dropzone" class="dropzone">
                    
                <div class="dz-message" style="height:200px;">
                    Coloca aqui imagenes del vehiculo 
                </div>
                <div class="dropzone-previews"></div>
                </div>
            </div>

            <div class="hr-line-dashed  col-lg-12"></div>
            <h3>Unidad</h3>

             <div class="form-group  col-lg-6">
                {!! Form::label('tipo_camion_id', 'Tipo') !!}
                {!! Form::select('tipo_camion_id', [], null, ['id' => 'tipoCam', 'class'=>'form-control', 'placeholder'=>'Select...', 'style' => 'width: 100%;']) !!}
            </div>

             <div class="form-group  col-lg-6">
                {!! Form::label('traccion_camion_id', 'Traccion') !!}
                {!! Form::select('traccion_camion_id', [], null, ['id' => 'traccionCam', 'class'=>'form-control', 'placeholder'=>'Select...', 'style' => 'width: 100%;']) !!}
            </div>

            <div class="form-group  col-lg-6">
                {!! Form::label('peso_camion_id', 'PBT(Ton)') !!}
                {!! Form::select('peso_camion_id', [], null, ['id' => 'pesoCam', 'class'=>'form-control', 'placeholder'=>'Select...', 'style' => 'width: 100%;']) !!}
            </div>

            <div id="sectionUnidadCarga" class="form-group  col-lg-6">
                {!! Form::label('capacidad_carga_camion_id', 'Capacidad Carga') !!}
                {!! Form::select('capacidad_carga_camion_id', [], null, ['id' => 'capaciCarga', 'class'=>'form-control', 'placeholder'=>'Select...', 'style' => 'width: 100%;']) !!}
            </div>

            <div class="form-group  col-lg-6">
                {!! Form::label('potencia_id', 'Potencia (CV)') !!}
                {!! Form::select('potencia_id', [], null, ['id' => 'potencia', 'class'=>'form-control', 'placeholder'=>'Select...', 'style' => 'width: 100%;']) !!}
            </div>

            <div class="form-group  col-lg-6">
                {!! Form::label('cilindrada_id', 'Cilindrada') !!}
                {!! Form::select('cilindrada_id', [], null, ['id' => 'cilindrada', 'class'=>'form-control', 'placeholder'=>'Select...', 'style' => 'width: 100%;']) !!}
            </div>

            <div class="form-group  col-lg-6">
                {!! Form::label('combustible', 'Combustible') !!}
                {!! Form::select('combustible', ['1' => 'Diesel', '2' => 'Nafta', '3' => 'GNC', '4' => 'Híbrido', '5' => 'Electrico', '6' => 'Otro'], null, ['class'=>'form-control', 'placeholder'=>'Select...', 'style' => 'width: 100%;']) !!}
            </div>

            <div class="form-group  col-lg-6">
                {!! Form::label('transmision_id', 'Transmisión') !!}
                {!! Form::select('transmision_id', [], null, ['id' => 'transmision', 'class'=>'form-control', 'placeholder'=>'Select...', 'style' => 'width: 100%;']) !!}
            </div>

            <div class="form-group  col-lg-6">
                {!! Form::label('direccion_id', 'Dirección') !!}
                {!! Form::select('direccion_id', [], null, ['id' => 'direccion', 'class'=>'form-control', 'placeholder'=>'Select...', 'style' => 'width: 100%;']) !!}
            </div>

            <div id="sectionUnidadTanque" class="form-group  col-lg-6">
                {!! Form::label('tanque_adicional_camion_id', 'Tanque Adicional') !!}
                {!! Form::select('tanque_adicional_camion_id', [], null, ['id' => 'tanque', 'class'=>'form-control', 'placeholder'=>'Select...', 'style' => 'width: 100%;']) !!}
            </div>


            <div id="sectionCabina">
                <div class="hr-line-dashed  col-lg-12"></div>
                <h3>Cabina</h3>
                <div class="row">
                    <div class="form-group  col-lg-6">
                        {!! Form::label('estado_cabina_id', 'Estado General') !!}
                        {!! Form::select('estado_cabina_id', ['1' => 'Malo', '2' => 'Regular', '3' => 'Bueno', '4' => 'Excelente'], null, ['class'=>'form-control', 'placeholder'=>'Select...', 'style' => 'width: 100%;']) !!}
                    </div>

                    <div class="form-group  col-lg-6">
                        {!! Form::label('dormitorio', 'Tipo') !!}
                        {!! Form::select('dormitorio', ['1' => 'Dormitorio', '2' => 'Extendida', '3' => 'Normal'], null, ['class'=>'form-control', 'placeholder'=>'Select...', 'style' => 'width: 100%;']) !!}
                    </div>

                    <div class="form-group  col-lg-6">
                        {!! Form::label('altura_cabina_id', 'Techo') !!}
                        {!! Form::select('altura_cabina_id', [], null, ['id'=>'altura','class'=>'form-control', 'placeholder'=>'Select...', 'style' => 'width: 100%;']) !!}
                    </div>

                    <div class="form-group  col-lg-6">
                        {!! Form::label('color_cabina_id', 'Color') !!}
                        {!! Form::select('color_cabina_id', [], null, ['id'=>'color','class'=>'form-control', 'placeholder'=>'Select...', 'style' => 'width: 100%;']) !!}
                    </div>

                    <div class="form-group  col-lg-12">
                        {!! Form::label('observaciones_cabina', 'Observaciones de la cabina') !!}
                        {!! Form::input('text', 'observaciones_cabina', null, ['class'=>'form-control', 'placeholder'=>'Enter...']) !!}
                    </div>
                </div>
            </div>

            <div class="hr-line-dashed  col-lg-12"></div>
            <h3>Carroceria</h3>

            

            <div id="sectionCarroceriaIfCarroceria" class="form-group  col-lg-12">
                {!! Form::label('ifCarroceria', 'Se vende con Carrocería ?') !!}
                {!! Form::select('ifCarroceria', ['1' => 'Si', '2' => 'No'], null, ['class'=>'form-control ifCarroceria', 'placeholder'=>'Select...', 'style' => 'width: 100%;', 'onChange' => 'showCarroceria()']) !!}
                
            </div>

                <div class="row fcarroceria" >
                    <div class="hr-line-dashed  col-lg-12"></div>
                    <div class="form-group  col-lg-6">
                        {!! Form::label('estado_carroceria_id', 'Estado General') !!}
                        {!! Form::select('estado_carroceria_id', ['1' => 'Malo', '2' => 'Regular', '3' => 'Bueno', '4' => 'Excelente'], null, ['class'=>'form-control', 'placeholder'=>'Select...', 'style' => 'width: 100%;']) !!}
                    </div>

                    <div id="sectionCarroceriaAsientos" class="form-group  col-lg-6" >
                        {!! Form::label('configuracion_carroceria_id', 'Asientos') !!}
                        {!! Form::select('configuracion_carroceria_id', [], null, ['id'=>'asientos','class'=>'form-control', 'placeholder'=>'Select...', 'style' => 'width: 100%;']) !!}
                    </div>

                    <div id="sectionCarroceriaBanio" class="form-group  col-lg-6" >
                        {!! Form::label('banio', 'Baño') !!}
                        {!! Form::select('banio', ['1' => 'Si', '2' => 'No'], null, ['class'=>'form-control', 'placeholder'=>'Select...', 'style' => 'width: 100%;']) !!}
                    </div>

                    <div id="sectionCarroceriaMiniBar" class="form-group  col-lg-6" >
                        {!! Form::label('minibar', 'MiniBar') !!}
                        {!! Form::select('minibar', ['1' => 'Si', '2' => 'No'], null, ['class'=>'form-control', 'placeholder'=>'Select...', 'style' => 'width: 100%;']) !!}
                    </div>

                    <div id="sectionCarroceriaRampa" class="form-group  col-lg-6" >
                        {!! Form::label('rampa', 'Rampa Discapacitado') !!}
                        {!! Form::select('rampa', ['1' => 'Si', '2' => 'No'], null, ['class'=>'form-control', 'placeholder'=>'Select...', 'style' => 'width: 100%;']) !!}
                    </div>

                    <div id="sectionCarroceriaBodega" class="form-group  col-lg-6" >
                        {!! Form::label('bodega', 'Bodega') !!}
                        {!! Form::select('bodega', ['1' => 'Si', '2' => 'No'], null, ['class'=>'form-control', 'placeholder'=>'Select...', 'style' => 'width: 100%;']) !!}
                    </div>

                    <div id="sectionCarroceriaTecho" class="form-group  col-lg-6" >
                        {!! Form::label('techo', 'Techo') !!}
                        {!! Form::select('techo', ['1' => 'Normal', '2' => 'Alta'], null, ['class'=>'form-control', 'placeholder'=>'Select...', 'style' => 'width: 100%;']) !!}
                    </div>

                    <div id="sectionCarroceriaObservaciones" class="form-group  col-lg-6" >
                        {!! Form::label('marca_carroceria_id', 'Marca') !!}
                        {!! Form::select('marca_carroceria_id', [], null, ['id'=>'marcaCarroceria','class'=>'form-control', 'placeholder'=>'Select...', 'style' => 'width: 100%;']) !!}
                    </div>

                    <div class="  col-lg-6">
                        {!! Form::label('observaciones_carroceria', 'Observaciones') !!}
                        {!! Form::input('text', 'observaciones_carroceria',null,   ['class'=>'form-control', 'placeholder'=>'Select...', 'style' => 'width: 100%;']) !!}
                    </div>

                    <div id="sectionCarroceriaTipo" class="form-group  col-lg-6">
                        {!! Form::label('tipo_carroceria_id', 'Tipo') !!}
                        {!! Form::select('tipo_carroceria_id', [], null, ['id'=>'tipoCarroceria' , 'class'=>'form-control', 'placeholder'=>'Select...', 'style' => 'width: 100%;']) !!}
                    </div>

                    <div id="sectionCarroceriaDimensiones" class="form-group  col-lg-6">
                        {!! Form::label('pago', 'Dimensiones') !!}
                        
                        {!! Form::input('text', 'alto_carroceria',null,   ['class'=>'form-control', 'placeholder'=>'Alto', 'style' => 'width: 100%;']) !!}
                        {!! Form::input('text', 'largo_carroceria',null,   ['class'=>'form-control', 'placeholder'=>'Largo', 'style' => 'width: 100%;']) !!}
                        {!! Form::input('text', 'ancho_carroceria',null,   ['class'=>'form-control', 'placeholder'=>'Ancho', 'style' => 'width: 100%;']) !!}
                    </div>

                    <div id="sectionCarroceriaAccesorios" class="form-group  col-lg-6">
                        {!! Form::label('accesorios_carroceria', 'Accesorios de Carrocería') !!}
                        {!! Form::input('text', 'accesorios_carroceria',null,   ['class'=>'form-control', 'placeholder'=>'Select...', 'style' => 'width: 100%;']) !!}
                    </div>
                </div>




            <div class="hr-line-dashed  col-lg-12"></div>
            <h3>Neumaticos</h3>
            <div class="hr-line-dashed  col-lg-12"></div>

            <div class="form-group  col-lg-6">
                {!! Form::label('desgaste_neumatico_id', 'Neumaticos') !!}
                {!! Form::select('desgaste_neumatico_id', [], null, ['id' => 'neumatico', 'class'=>'form-control', 'placeholder'=>'Select...', 'style' => 'width: 100%;']) !!}
            </div>

            <div class="form-group  col-lg-6">
                {!! Form::label('medida_neumaticos', 'Medidas') !!}
                {!! Form::input('text', 'medida_neumaticos', null, ['class'=>'form-control', 'placeholder'=>'Enter...', 'id' => 'title']) !!}
            </div>

            <div class="form-group  col-lg-6">
                {!! Form::label('recapado_neumaticos', 'Racapadas') !!}
                {!! Form::select('recapado_neumaticos', ['1' => 'Si', '2' => 'No'], null, ['class'=>'form-control', 'placeholder'=>'Select...', 'style' => 'width: 100%;']) !!}
            </div>

            <div class="form-group  col-lg-6">
                {!! Form::label('auxilio', 'Auxilio') !!}
                {!! Form::select('auxilio', ['1' => 'Si', '2' => 'No'], null, ['class'=>'form-control', 'placeholder'=>'Select...', 'style' => 'width: 100%;']) !!}
            </div>

            <h3>Equipamiento</h3>
            <div class="hr-line-dashed  col-lg-12"></div>

            <div class="form-group  col-lg-6">
                {!! Form::label('accesorios', 'Accesorio') !!}
                {!! Form::select('accesorios', [ ], null, ['id'=>'accesorio', 'class'=>'form-control', 'placeholder'=>'Select...', 'multiple' => 'multiple', 'style' => 'width: 100%;']) !!}
            </div>

            <div class="form-group  col-lg-6">
                {!! Form::label('confort', 'Confort') !!}
                {!! Form::select('confort', [ ], null, ['id'=>'confort','class'=>'form-control', 'placeholder'=>'Select...', 'multiple' => 'multiple', 'style' => 'width: 100%;']) !!}
            </div>

            <div class="form-group  col-lg-6">
                {!! Form::label('seguridad', 'Seguridad') !!}
                {!! Form::select('seguridad', [ ], null, ['id'=>'seguridad','class'=>'form-control', 'placeholder'=>'Select...', 'multiple' => 'multiple', 'style' => 'width: 100%;']) !!}
            </div>

            <div class="form-group  col-lg-6">
                {!! Form::label('sonido', 'Sonido') !!}
                {!! Form::select('sonido', [ ], null, ['id'=>'sonido','class'=>'form-control', 'placeholder'=>'Select...', 'multiple' => 'multiple', 'style' => 'width: 100%;']) !!}
            </div>


            <h3>Extras</h3>
            <div class="form-group  col-lg-12">
                {!! Form::label('observaciones', 'Observaciones') !!}
                {!! Form::input('text','observaciones', null,  ['class'=>'form-control', 'placeholder'=>'Select...', 'style' => 'width: 100%;']) !!}
            </div>

            
            <div class="hr-line-dashed  col-lg-12"></div>
            <div class="form-group  col-lg-6">
                {!! Form::submit('Publicar', ['class'=> 'btn btn-primary', 'id' => 'submit']) !!}
            </div>
        </div>
    </div>
</div>






@section('inline_scripts')

    {!! Html::script('js/dropzone.js'); !!}


    <script>
        Dropzone.options.myDropzone = {
            url: "file/",
            autoProcessQueue: false,
            uploadMultiple: true,
            maxFilezise: 10,
            addRemoveLinks: true,
            // maxFiles: 2,
            
            init: function() {
                var submitBtn = document.querySelector("#submit");
                myDropzone = this;
                
                submitBtn.addEventListener("click", function(e){
                    e.preventDefault();
                    e.stopPropagation();
                    myDropzone.processQueue();
                });
                this.on("addedfile", function(file) {
                    
                });
                
                this.on("complete", function(file) {
                    myDropzone.removeFile(file);

                    let params = $('form').serialize();
                    $.post('vehicles/', params, function(result){
                        if(result.cod == "WS001"){
                            alert(result.message);
                            window.location.replace("listings/");
                        }
                    })
                });
 
                this.on("success", 
                    myDropzone.processQueue.bind(myDropzone)
                );
            }
        };
    </script>




    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={!! env('GOOGLE_PLACES_API_KEY') !!}&libraries=places"></script>

    <script type="text/javascript">
        var input = document.getElementById('location');
        autocomplete = new google.maps.places.Autocomplete(input, {});

        // Bias the autocomplete object to the user's geographical location,
        // as supplied by the browser's 'navigator.geolocation' object.
        function geolocate() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    var geolocation = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    var circle = new google.maps.Circle({
                        center: geolocation,
                        radius: position.coords.accuracy
                    });
                    autocomplete.setBounds(circle.getBounds());
                });
            }
        }

        function nextSheet(int){


            // sectionCabina = display none

            // sectionCarroceriaAsientos = display none
            // sectionCarroceriaBanio = display none
            // sectionCarroceriaMiniBar = display none
            // sectionCarroceriaRampa = display none
            // sectionCarroceriaBodega = display none
            // sectionCarroceriaObservaciones = display none
            
            // sectionCarroceriaDimensiones = display none
            // sectionCarroceriaAccesorios = display none
            // sectionCarroceriaTipo = display none
            // sectionCarroceriaIfCarroceria = display none

            // sectionCarroceriaTecho = display none



            switch($('#typeVehicle').val()){
                case '1': 
                        $('#sectionCarroceriaAsientos').show();
                        $('#sectionCarroceriaBanio').show();
                        $('#sectionCarroceriaMiniBar').show();
                        $('#sectionCarroceriaRampa').show();
                        $('#sectionCarroceriaBodega').show();
                        $('#sectionCarroceriaObservaciones').show();
                        break;  
                case '2': 
                        $('#sectionCabina').show();
                        $('#sectionUnidadCarga').show();
                        $('#sectionUnidadTanque').show();
                        $('#sectionCarroceriaDimensiones').show();
                        $('#sectionCarroceriaAccesorios').show();
                        $('#sectionCarroceriaTipo').show();
                        $('#sectionCarroceriaIfCarroceria').show();
                        break;
                case '3': 
                        $('#sectionUnidadCarga').show();
                        $('#sectionCarroceriaTecho').show();
                        break;
            }


            switch(int){
                case 1: $('.fve').show(); 
                        break;
                case 2: 
                        

                        var url = 'vehicles/filter';

                        let params = {
                            type: $('#typeVehicle').val(),
                            step: 2,
                        } 
                        
                        $.get(url,params,function(resul){

                            $('.fve').hide();
                        
                            $('.ftecnicacam').show();

                            $('#title').val($('#marca :selected').html() + ' - ' + $('#modelo :selected').html() + ' - ');
                            $('#tipoCam').html('')
                            resul.params.tipo.forEach(function(item){
                                $('#tipoCam').append("<option value='"+item.id+"'>"+item.name+"</option>")
                            })

                            $('#pesoCam').html('')
                            resul.params.peso.forEach(function(item){
                                $('#pesoCam').append("<option value='"+item.id+"'>"+item.name+"</option>")
                            })

                            $('#neumatico').html('')
                            resul.params.neumatico.forEach(function(item){
                                $('#neumatico').append("<option value='"+item.id+"'>"+item.name+"</option>")
                            })


                            $('#direccion').html('')
                            resul.params.direccion.forEach(function(item){
                                $('#direccion').append("<option value='"+item.id+"'>"+item.name+"</option>")
                            })

                            $('#cilindrada').html('')
                            resul.params.cilindrada.forEach(function(item){
                                $('#cilindrada').append("<option value='"+item.id+"'>"+item.name+"</option>")
                            })

                            $('#transmision').html('')
                            resul.params.transmision.forEach(function(item){
                                $('#transmision').append("<option value='"+item.id+"'>"+item.name+"</option>")
                            })

                            $('#traccionCam').html('')
                            resul.params.traccion.forEach(function(item){
                                $('#traccionCam').append("<option value='"+item.id+"'>"+item.name+"</option>")
                            })

                            $('#potencia').html('')
                            resul.params.potencia.forEach(function(item){
                                $('#potencia').append("<option value='"+item.id+"'>"+item.name+"</option>")
                            })
                            switch($('#typeVehicle').val()){
                                case '1':
                                    $('#asientos').html('')
                                    resul.params.configuracion.forEach(function(item){
                                        $('#asientos').append("<option value='"+item.id+"'>"+item.name+"</option>")
                                    })

                                    $('#marcaCarroceria').html('')
                                    resul.params.marcaCarroceria.forEach(function(item){
                                        $('#marcaCarroceria').append("<option value='"+item.id+"'>"+item.name+"</option>")
                                    })


                                    break;
                                case '2':
                                    $('#tipoCarroceria').html('')
                                    resul.params.aplicacion.forEach(function(item){
                                        $('#tipoCarroceria').append("<option value='"+item.id+"'>"+item.name+"</option>")
                                    })

                                    $('#altura').html('')
                                    resul.params.altura.forEach(function(item){
                                        $('#altura').append("<option value='"+item.id+"'>"+item.name+"</option>")
                                    })

                                    $('#color').html('')
                                    resul.params.color.forEach(function(item){
                                        $('#color').append("<option value='"+item.id+"'>"+item.name+"</option>")
                                    })

                                    $('#capaciCarga').html('')
                                    resul.params.carga.forEach(function(item){
                                        $('#capaciCarga').append("<option value='"+item.id+"'>"+item.name+"</option>")
                                    })

                                    $('#fechav').html('')
                                    resul.params.fecha.forEach(function(item){
                                        $('#fechav').append("<option value='"+item.id+"'>"+item.name+"</option>")
                                    })

                                    $('#tanque').html('')
                                    resul.params.tanque.forEach(function(item){
                                        $('#tanque').append("<option value='"+item.id+"'>"+item.name+"</option>")
                                    })
                                        break;
                                case '3':
                                    $('#capaciCarga').html('')
                                    resul.params.carga.forEach(function(item){
                                        $('#capaciCarga').append("<option value='"+item.id+"'>"+item.name+"</option>")
                                    })
                                        break;
                            }







                            $('#accesorio').html('')
                            $('#confort').html('')
                            $('#seguridad').html('')
                            $('#sonido').html('')
                            
                            resul.params.equipamiento.forEach(function(item){
                                switch(item.type){
                                    case "Equipamiento": 
                                        $('#accesorio').append("<option value='"+item.id+"'>"+item.name+"</option>")
                                        break;
                                    case "Confort": 
                                        $('#confort').append("<option value='"+item.id+"'>"+item.name+"</option>")
                                        break;
                                    case "Seguridad": 
                                        $('#seguridad').append("<option value='"+item.id+"'>"+item.name+"</option>")
                                        break;
                                    case "Audio y Video": 
                                        $('#sonido').append("<option value='"+item.id+"'>"+item.name+"</option>")
                                        break;

                                }
                                
                            })



                        })

                        break;
            }
        }

        function showCarroceria(){
            $('#ifCarroceria').val() == 1 ? $('.fcarroceria').show():$('.fcarroceria').hide();
        }

        function loadYear(){
             $('#condicion').val() == 1 ? $('.datepicker').val(new Date().getFullYear() + 1) : $('.datepicker').val('');
        }

        function loadMarcas() {
            var url = 'vehicles/filter';

            let params = {
                type: $('#typeVehicle').val(),
            } 
            
            $.get(url,params,function(resul){
                $('#marca').html('')
                resul.marcas.forEach(function(item){
                    $('#marca').append("<option value='"+item.id+"'>"+item.name+"</option>")
                })
            })
        }

        function loadModelos() {
            
            var url = 'vehicles/filter';

            let params = {
                type: $('#typeVehicle').val(),
                marca: $('#marca').val(),
            } 
            
            $.get(url,params,function(resul){
                $('#modelo').html('')
                resul.modelos.forEach(function(item){
                    $('#modelo').append("<option value='"+item.id+"'>"+item.name+"</option>")
                })
            })
            
        }

        function loadAppUso() {
            var url = 'vehicles/filter';

            let params = {
                type: $('#typeVehicle').val(),
                modelo: $('#modelo').val(),
            } 
            
            $.get(url,params,function(resul){
                $('#aplicacion').html('')
                resul.aplicacionUso.forEach(function(item){
                    $('#aplicacion').append("<option value='"+item.id+"'>"+item.name+"</option>")
                })
            })
        }

        $(document).ready(function () {

           
            $('.ftecnicacam').hide();
          

            var $form = $('.ibox form');
            $form
                    .find('select').select2().end()
                    .find('.datepicker').datetimepicker({
                        format: 'Y',
                        sideBySide: true
                    }).end()
                    .find('#teaser').summernote({ height: 200 }).end()
                    .find('#body').summernote({ height: 500 }).end()
                    .formValidation({
                        framework: 'bootstrap',
                        icon: {
                            invalid: 'glyphicon glyphicon-remove',
                            validating: 'glyphicon glyphicon-refresh'
                        },
                        fields: {
                            'medida_neumaticos': {
                                validators: {
                                    notEmpty: {},
                                    stringLength: {min: 3, max: 255}
                                }
                            },
                            'created_by': {
                                validators: {
                                    notEmpty: {}
                                }
                            },
                            'location': {
                                validators: {
                                    notEmpty: {}
                                }
                            }
                        },
                        excluded: ':disabled'
                    });
        });
    </script>
@endsection






        
    
