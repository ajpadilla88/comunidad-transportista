@extends('account.layouts.page')

@section('page_title') Perfil @stop

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{!! route('root') !!}">Home</a></li>
        <li><b>Perfil</b></li>
    </ol>
@stop

@section('content')
<div class="ibox">
    {!! Form::model($user, ['route' => ['account.user.profile.update', $user->id], 'method' => 'patch']) !!}
    <div class="ibox-title">
        <h3>Datos</h3>
    </div>
    <div class="ibox-content">
        <div class="row">
            {{--<div class="form-group col-lg-3">--}}
            {{--{!! Form::label('concesionario_id', 'Concesionario') !!}--}}
            {{--{!! Form::select('concesionario_id', $concesionarios, null, ['class'=>'form-control', 'placeholder'=>'Select...', 'style' => 'width: 100%;', 'id' => 'concesionario_id', 'onChange' => '']) !!}--}}
            {{--</div>--}}
            <div class="form-group  col-lg-3">
            {!! Form::label('first_name', 'Nombre') !!}
            {!! Form::input('text', 'first_name', null, ['class'=>'form-control', 'placeholder'=>'Enter...', 'id' => '', 'onFocus' => ""]) !!}
            </div>
            <div class="form-group  col-lg-3">
            {!! Form::label('last_name', 'Apellido') !!}
            {!! Form::input('text', 'last_name', null, ['class'=>'form-control', 'placeholder'=>'Enter...', 'id' => '', 'onFocus' => ""]) !!}
            </div>
            <div class="form-group  col-lg-3">
            {!! Form::label('email', 'Email') !!}
            {!! Form::input('text', 'email', null, ['class'=>'form-control', 'placeholder'=>'Enter...', 'id' => '', 'onFocus' => ""]) !!}
            </div>
            <div class="form-group  col-lg-3">
            {!! Form::label('telefono', 'Teléfono') !!}
            {!! Form::input('text', 'phone', null, ['class'=>'form-control', 'placeholder'=>'Enter...', 'id' => '', 'onFocus' => ""]) !!}
            </div>
            <div class="form-group col-lg-3">
            {!! Form::label('state_id', 'Provincia') !!}
            {!! Form::select('state_id', $states, $user->state_id, ['class'=>'form-control', 'placeholder'=>'Select...', 'style' => 'width: 100%;', 'id' => 'state_id', 'onChange' => '']) !!}
            </div>
            <div class="form-group  col-lg-3">
            {!! Form::label('postal_code', 'Código Postal') !!}
            {!! Form::input('text', 'postal_code', null, ['class'=>'form-control', 'placeholder'=>'Enter...', 'id' => '', 'onFocus' => ""]) !!}
            </div>
        </div>
    </div>
</div>
<div class="ibox">
     <div class="ibox-title">
        <h3>Cambiar Contraseña</h3>
    </div>
    <div class="ibox-content">
        <div class="row">
            <div class="form-group  col-lg-3">
            {!! Form::label('password', 'Contraseña') !!}
            {!! Form::input('password', 'password', null, ['class'=>'form-control', 'placeholder'=>'Enter...', 'id' => '', 'onFocus' => ""]) !!}
            </div>
            <div class="form-group  col-lg-3">
            {!! Form::label('password_confirmation', 'Confirmar Contraseña') !!}
            {!! Form::input('password', 'password_confirmation', null, ['class'=>'form-control', 'placeholder'=>'Enter...', 'id' => '', 'onFocus' => ""]) !!}
            </div>
            <div class="hr-line-dashed  col-lg-12"></div>
            <div class="form-group  col-lg-6">
                {!! Form::submit('Actualizar', ['class'=> 'btn btn-primary']) !!}
            </div>
        </div>
    </div>
</div>
    {!! Form::close() !!}
@stop