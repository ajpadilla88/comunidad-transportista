@extends('front.layouts.page')

@section('page_title') Eventos @stop

@section('content')
    @include('front.layouts.nav')

    <style>
        body {
            height: 100%;
        }
        section.events {
            background-color: #fff;
            height: 100vh;
            display: inline-block;
            width: 100%;
        }
        section.events .first {
            width: 80%;
            height: 100%;
            float: left;
        }
        section.events .sidebar {
            width: 20%;
            float: right;
            height: 100%;
            padding: 10px;
            background-color: white;
            padding: 15px;
        }

        section.events .sidebar h3 {
            margin-top: 0;
            font-family: 'PT Sans Narrow', sans-serif;
            font-size: 30px;
        }

        section.events .sidebar p {
            font-family: 'Open Sans', sans-serif;
            font-weight: 300;
        }

        #map {
            width: 100%;
            height: 100%;
        }
    </style>

    <section class="events">
        <div class="first">
            <div id="map"></div>
            <script>
                function initMap() {
                    // Create a map object and specify the DOM element for display.
                    var map = new google.maps.Map(document.getElementById('map'), {
                        center: {lat: -34.6155729, lng: -58.5033603},
                        scrollwheel: false,
                        zoom: 10
                    });
                }

            </script>
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCDNsIYE7BlOvXV736ltSQPFgje6zaZDsA&callback=initMap" async defer></script>
        </div>
        <div class="sidebar">
            <h3>Localizamos todas nuestras acciones</h3>
            <p>Estamos trabajando para visibilizar todas las actividades de la red y que te conectes con la más cercana.</p>

            {{--<h3>Reserva Ecológica Pilar</h3>--}}
            {{--<p>Esta es la dirección del lugar o lo que se haya ingresado como locación.</p>--}}
        </div>
    </section>



    @include('front.layouts.footer')

@stop