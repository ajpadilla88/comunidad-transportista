@extends('front.layouts.page')

@section('page_title') Acerca @stop

@section('content')
    @include('front.layouts.nav')

    <section class="cover" style="background-image: url('/img/arboles.jpg');">
        <h1>Acerca de Alianza Arboles</h1>
    </section>

    <section class="acerca">
        <div class="container-fluid">
            <div class="container p-lg">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="body">
                            <p>
                                Alianza Arboles es una red que reúne individuos, organizaciones que trabajan con inclusión
                                social, instituciones educativas, reservas naturales y empresas con el objetivo común de crear y
                                mejorar espacios verdes públicos.</p>

                            <p> Tejemos lazos de cooperación y empoderamos a las personas para que sean protagonistas en su
                                comunidad.</p>

                            <p> Construímos ciudadanía. Fomentamos prácticas democráticas, vinculantes, inclusivas, pacíficas y
                                cotidianas para construir una sociedad más inclusiva, sana y en paz.</p>

                            <p> Espacios verdes públicos:Los espacios verdes nos ayudan a vincularnos, son un lugar de encuentro
                                y una muestra de nuestra biodiversidad, nos acercan a la naturaleza.</p>

                            <p> Inclusión social: Invitamos a participar a quienes en el día a día no tienen la posibilidad de
                                ser protagonistas en su comunidad, para que se sumen a nuestra red desde su lugar y vivan la
                                alegría de ser voluntarios.</p>

                            <p> Educación sustentable: Por medio de la acción experimental y vivencial, acercamos nuestro
                                proyecto a jardines de infantes, escuelas y universidades para que se sumen a esta red que
                                trabaja por el cuidado del medio ambiente y la inclusión social.</p>

                            <p> Participación ciudadana: Todos podemos participar, donando semillas de árboles nativos, haciendo
                                o adoptando los plantines, donando materiales y enriqueciendo espacios verdes para nuestros
                                vecinos.</p>

                            <p> Para más información, escribinos a <a href="mailto:info@alianzaarboles.org">info@alianzaarboles.org</a>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="counters">
                    <p class="lead">Proyectos de la comunidad de Alianza Arboles</p>
                    <div class="counter arboles">
                        <span class="triangle"></span>
                        <span class="icon"><img src="/img/iconos/arbol.svg"></span>
                        <span class="count">4600</span>
                        <span class="name">Arboles plantados</span>
                    </div>

                    <div class="counter plantines">
                        <span class="triangle"></span>
                        <span class="icon"><img src="/img/iconos/plantin.svg"></span>
                        <span class="count">6837</span>
                        <span class="name">Plantines sembrados</span>
                    </div>

                    <div class="counter herramientas">
                        <span class="triangle"></span>
                        <span class="icon"><img src="/img/iconos/herramientas.svg"></span>
                        <span class="count">2162</span>
                        <span class="name">Herramientas donadas</span>
                    </div>

                    <div class="counter banco-plantines">
                        <span class="triangle"></span>
                        <span class="icon"><img src="/img/iconos/banco.svg"></span>
                        <span class="count">30</span>
                        <span class="name">Banco de plantines</span>
                    </div>

                    <div class="counter jardines">
                        <span class="triangle"></span>
                        <span class="icon"><img src="/img/iconos/jardin.svg"></span>
                        <span class="count">10</span>
                        <span class="name">Jardines Botánicos Comunitarios</span>
                    </div>

                    <div class="counter voluntarios">
                        <span class="triangle"></span>
                        <span class="icon"><img src="/img/iconos/voluntarios.svg"></span>
                        <span class="count">4208</span>
                        <span class="name">Voluntarios Sumados</span>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('front.layouts.footer')

@stop

@section('inline_styles')
    <style type="text/css">



    </style>
@endsection