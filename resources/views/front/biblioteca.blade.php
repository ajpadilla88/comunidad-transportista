@extends('front.layouts.page')

@section('page_title') Biblioteca @stop

@section('content')
    @include('front.layouts.nav')

    <section class="cover" style="background-image: url('/img/arboles.jpg');">
        <h1>Biblioteca</h1>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="container">


                <div class="grid">
                    <div class="grid-sizer"></div>
                    <div class="grid-item">
                        <div class="box box-full">
                            <img class="img-responsive" src="/img/box-podeshacer.png">
                            <div class="teaser">
                                <h2>Mirá nuestra breve publicación online</h2>
                                <p>Encontrá la acción que más te entusiasme y te mostramos como empezar.</p>
                            </div>
                        </div>
                    </div>

                    <div class="grid-item">
                        <div class="box box-full">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" width="1280" height="720" src="https://www.youtube.com/embed/5hCDRrBCNmI?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                            </div>
                            <div class="teaser green">
                                <h2>Video Tutorial de Jardines Botánicos Comunitarios</h2>
                                <p>Conocé más sobre cómo crear un jardín botánico comunitario y por qué es importante.</p>
                            </div>
                        </div>
                    </div>

                    <div class="grid-item">
                        <div class="box box-full">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" width="1280" height="720" src="https://www.youtube.com/embed/-kvgq0zQjcw?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                            </div>
                            <div class="teaser green">
                                <h2>Video Tutorial de Plantines</h2>
                                <p>Empezá tu propio banco de plantines para cuidar o donar.</p>
                            </div>
                        </div>
                    </div>

                    <div class="grid-item">
                        <div class="box box-full">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" width="1280" height="720" src="https://www.youtube.com/embed/-66UKLViaZM?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                            </div>
                            <div class="teaser green">
                                <h2>Mirá el día en que creamos un Jardín Botánico Comunitario en un Hospital de Niños</h2>
                            </div>
                        </div>
                    </div>

                    <div class="grid-item">
                        <div class="box box-full">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" width="1280" height="720" src="https://www.youtube.com/embed/Hv1espjVBKg?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                            </div>
                            <div class="teaser green">
                                <h2>Mirá el día en que creamos un Jardín Botánico Comunitario en una Residencia para Mayores</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('front.layouts.footer')

@stop

@section('inline_scripts')
    <script>
        jQuery(function($) {
            $('.grid').masonry({
                // options...
                itemSelector: '.grid-item',
                columnWidth: '.grid-sizer',
                percentPosition: true
            });
        });
    </script>
@append

@section('inline_styles')
    <style>
        section.content .grid .grid-sizer,
        section.content .grid .grid-item {
            width: 33.3%;
            padding: 0.5%;
        }

        @media (max-width: 768px) {
            section.content .grid .grid-sizer,
            section.content .grid .grid-item {
                width: 100%;
                padding: 0;
                margin-bottom: 30px;
            }
        }
    </style>
@append