@extends('front.layouts.page')

@section('page_title') Camiones @stop

@section('content')
    @include('front.layouts.nav')

    <nav class="subheader camiones">
        <h2>Comunidad Camiones</h2>
        <span><img src="/img/camion.png"></span>
    </nav>
    <div class="flex">
        <aside class="filters">
            {{--<h3>Refina tu búsqueda</h3>--}}

            <form action="/searchTrucks" method="get">
                <!--<input type="hidden" name="_token" value="{{ csrf_token() }}">-->
                <div class="form-group">
                    <input type="text" class="form-control search" name="keyWords" id="keywords" placeholder="Palabras clave">
                </div>

                <div class="filter-group">
                    <h4>Precio</h4>
                    <input type="text" name="priceRange" id="priceRange1" value="" placeholder="Rango 1">
                    <input type="text" name="priceRange2" id="priceRange2" value="" placeholder="Rango 2">
                    <!--<a href="#">Rango 1 <span class="small">(42)</span></a>
                    <a href="#">Rango 2 <span class="small">(42)</span></a>
                    <a href="#">Rango 3 <span class="small">(42)</span></a>
                    <a href="#">Rango 4 <span class="small">(42)</span></a>-->
                </div>

                <div class="filter-group">
                    <h4>Ubicación</h4>
                    <!--<a href="#">Provincia A <span class="small">(42)</span></a>
                    <a href="#">Provincia B <span class="small">(42)</span></a>-->
                    {!! Form::select('location', $states, null, ['class'=>'form-control', 'placeholder'=>'Select...', 'style' => 'width: 100%;', 'id' => 'state_id', 'onChange' => '']) 
                    !!}
                </div>

                <div class="filter-group">
                    <h4>Aplicación de Uso</h4>
                    <!--<a href="#">Rango 1 <span class="small">(42)</span></a>
                    <a href="#">Rango 2 <span class="small">(42)</span></a>
                    <a href="#">Rango 3 <span class="small">(42)</span></a>
                    <a href="#">Rango 4 <span class="small">(42)</span></a>-->
                    {!! Form::select('applicationForUse', $applicationForUse, null, ['class'=>'form-control', 'placeholder'=>'Select...', 'style' => 'width: 100%;', 'id' => 'applicationForUse', 'onChange' => '']) 
                    !!}
                </div>

                <!--<div class="filter-group">
                    <h4>Etcéteras</h4>
                    <a href="#">Etc 1 <span class="small">(42)</span></a>
                    <a href="#">Etc 2 <span class="small">(42)</span></a>
                    <a href="#">Etc 3 <span class="small">(42)</span></a>
                    <a href="#">Etc 4 <span class="small">(42)</span></a>
                </div>-->

                <input type="submit" value="Search" class="btn btn-primary">            
                </form>
        </aside>

        <main class="content">

            {{-- @for($i = 0; $i < 15; $i++)
            <div class="box">
                <img src="/img/mock-car.jpg" class="img-responsive">
                <span class="pull-right price">$ 300.000</span>
                <h3>IVECO, EURO TECH 190E31</h3>
                <hr>
                <div class="attribute"><label>Ubicación:</label> Buenos Aires - GBA Norte</div>
                <div class="attribute"><label>Aplicación de uso:</label> Mudanza</div>
                <div class="attribute"><label>KMs:</label> 0</div>
                <a href="/camiones/test" class="btn btn-primary m-t">Ver detalles</a>
            </div>
            @endfor --}}

            @isset ($trucks)
                @foreach ($trucks as $truck)
                <div class="box">
                    <img src="/img/mock-car.jpg" class="img-responsive">
                    <span class="pull-right price">{{ $truck->price }}</span>
                    <h3>{{ $truck->title }}</h3>
                    <hr>
                    <div class="attribute"><label>Ubicación:</label> {{ $truck->location->name }}</div>
                    <div class="attribute"><label>Aplicación de uso:</label> {{ $truck->applicationForUse->name }}</div>
                    <div class="attribute"><label>KMs:</label> 0</div>
                    <a href="/camiones/test" class="btn btn-primary m-t">Ver detalles</a>
                </div>
                @endforeach
            @endisset

        </main>
    </div>

    @include('front.layouts.footer')
@stop

@section('inline_styles')
    <style>
        .flex {
            display: flex;
        }

        aside.filters {
            float: left;
            width: 20%;
            padding: 25px;
            min-height: 300px;
            -webkit-box-shadow: 2px 1px 22px 0px rgba(0, 0, 0, 0.1);
            -moz-box-shadow: 2px 1px 22px 0px rgba(0, 0, 0, 0.1);
            box-shadow: 2px 1px 22px 0px rgba(0, 0, 0, 0.1);
        }

        aside.filters input.search {
            -webkit-border-radius: 0;
            -moz-border-radius: 0;
            border-radius: 0;
            height: auto;
            padding: 8px 10px;
            border: 1px solid #ccc;
        }

        aside.filters label {
            font-weight: bold;
        }
        
        aside.filters .filter-group {
            margin-bottom: 20px;
        }
        aside.filters .filter-group  h4 {
            font-size: 15px;
            padding: 5px 0;
            border-bottom: 1px solid;
            font-family: 'Open Sans', sans-serif;
            font-weight: 600;
        }
        aside.filters .filter-group a {
            display: block;
            color: inherit;
        }

        aside.filters .filter-group a span.small {
            font-size: 10px;
        }

        main.content {
            width: 100%;
            position: relative;
            padding: 20px;
        }

        main.content .box {
            display: inline-block;
            width: 33%;
            padding: 10px;
        }

        main.content .box img {
            margin-bottom: 15px;
        }

        main.content .box h3 {
            color: #43bb73;
            margin: 0;
            font-size: 16px;
            font-weight: bold;
            line-height: 1em;
        }

        main.content .box span.price {
            font-size: 16px;
            font-weight: bold;
            line-height: 1em;
        }

        main.content .box hr {
            border-top: 1px solid #c9c9c9;
            margin: 15px 0;
        }

        main.content .box .attribute {
            font-size: 12px;
        }

        main.content .box label {
            color: #999;
        }
        
        main.content a.btn {
            background-color: #d93649;
            -webkit-border-radius: 0;
            -moz-border-radius: 0;
            border-radius: 0;
            border: #d93649;
        }

    </style>
@endsection

@section('inline_scripts')
    <script>
        $( document ).ready(function() {
            var inputSearch = {
                'keyWords': '', 
                'priceRange': {'range1': '', 'range2': ''}, 
                'location': '', 
                'applicationForUse': ''
            };

            $("#keywords").keyup(function(){
                inputSearch.keyWords = this.value;
                 $.ajax({
                    type: 'GET',
                    url:'/searchTrucks',
                    data: inputSearch,
                    dataType: "JSON",
                    success: function(response) {
                        console.log(response);
                    }
                });
             });

            $("#priceRange1").keyup(function(){
                inputSearch.priceRange.range1 = this.value;
                $.ajax({
                    type: 'GET',
                    url:'/searchTrucks',
                    data: inputSearch,
                    dataType: "JSON",
                    success: function(response) {
                        console.log(response);
                    }
                });
            });

            $("#priceRange2").keyup(function(){
                inputSearch.priceRange.range2  = this.value;
                $.ajax({
                    type: 'GET',
                    url:'/searchTrucks',
                    data: inputSearch,
                    dataType: "JSON",
                    success: function(response) {
                        console.log(response);
                    }
                });
            });

            $("#state_id").change(function() {
                inputSearch.location = this.value;
                $.ajax({
                    type: 'GET',
                    url:'/searchTrucks',
                    data: inputSearch,
                    dataType: "JSON",
                    success: function(response) {
                        console.log(response);
                    }
                });
            });

            $("#applicationForUse").change(function() {
                inputSearch.applicationForUse = this.value;
                $.ajax({
                    type: 'GET',
                    url:'/searchTrucks',
                    data: inputSearch,
                    dataType: "JSON",
                    success: function(response) {
                        console.log(response);
                    }
                });
            });

        });
    </script>

@append