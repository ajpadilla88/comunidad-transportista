@extends('front.layouts.page')

@section('page_title') Home @stop

@section('content')
    @include('front.layouts.nav')

    <div class="container-fluid">
        <section id="hero">
            <div id="slider" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#slider" data-slide-to="0" class="active"></li>
                    <li data-target="#slider" data-slide-to="1"></li>
                    <li data-target="#slider" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="/img/imagen1.png" alt="">

                        <div class="carousel-caption"></div>
                    </div>
                    <div class="item">
                        <img src="/img/imagen2.jpg" alt="">

                        <div class="carousel-caption"></div>
                    </div>
                    <div class="item">
                        <img src="/img/imagen3.jpg" alt="">

                        <div class="carousel-caption"></div>
                    </div>
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#slider" role="button" data-slide="prev">
                    <span style="top: 40%; left: 20%; position:absolute;"><img src="/img/home-cc-slide-boton-izquierdo.png" width="60"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#slider" role="button" data-slide="next">
                    <span style="top: 40%; right: 20%; position:absolute;"><img src="/img/home-cc-slide-boton-derecho.png" width="60"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </section>
    </div>
    <div class="container">

        <hr>

        <section class="choose">
            <h2 class="">¿Qué tipo de vehículo buscas?</h2>

            <div class="container">
                <div class="row">
                    <div class="col-md-4 p-lg box"><a href="/camiones"><img alt="" src="/img/camiones.png"></a></div>
                    <div class="col-md-4 p-lg box"><a href="/buses"><img alt="" src="/img/buses.png"></a></div>
                    <div class="col-md-4 p-lg box"><a href="/utilitarios"><img alt="" src="/img/utilitarios.png"></a>
                    </div>
                </div>
            </div>

        </section>

        <hr>

        <section class="concesionarios-destacados">
            <div class="well well-lg">
                <h2 class="m-b-lg">Concesionarios destacados</h2>

                <div id="thumb-slider" class="carousel slide">
                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="row">
                                <div class="col-sm-2">
                                    <a href="#x"><img src="/img/colcar.jpeg" alt="Image" class="img-responsive"></a>
                                </div>
                                <div class="col-sm-2">
                                    <a href="#x"><img src="/img/colcar.jpeg" alt="Image" class="img-responsive"></a>
                                </div>
                                <div class="col-sm-2">
                                    <a href="#x"><img src="/img/colcar.jpeg" alt="Image" class="img-responsive"></a>
                                </div>
                                <div class="col-sm-2">
                                    <a href="#x"><img src="/img/colcar.jpeg" alt="Image" class="img-responsive"></a>
                                </div>
                                <div class="col-sm-2">
                                    <a href="#x"><img src="/img/colcar.jpeg" alt="Image" class="img-responsive"></a>
                                </div>
                                <div class="col-sm-2">
                                    <a href="#x"><img src="/img/colcar.jpeg" alt="Image" class="img-responsive"></a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-sm-2">
                                    <a href="#x"><img src="/img/colcar.jpeg" alt="Image" class="img-responsive"></a>
                                </div>
                                <div class="col-sm-2">
                                    <a href="#x"><img src="/img/colcar.jpeg" alt="Image" class="img-responsive"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a class="left carousel-control" href="#thumb-slider" data-slide="prev"></a>
                    <a class="right carousel-control" href="#thumb-slider" data-slide="next"></a>
                </div>
            </div>
        </section>

        <hr>

        <section>
            <h2 class="m-b-lg"><span class="m-r"><img src="/img/icon-noticias.png"></span>Últimas cargas</h2>

            <div class="panel featured" style="display: flex;">
                <div clas="row">
                    <div class="col-md-4">
                        <img class="img-responsive" src="/img/mock-car.jpg">

                        <h3>2006 Saab prueba 2 Utilitario</h3>
                        <h4>$ 45,634</h4>
                    </div>
                    <div class="col-md-4">
                        <img class="img-responsive" src="/img/mock-car.jpg">

                        <h3>2006 Saab prueba 2 Utilitario</h3>
                        <h4>$ 45,634</h4>
                    </div>
                    <div class="col-md-4">
                        <img class="img-responsive" src="/img/mock-car.jpg">

                        <h3>2006 Saab prueba 2 Utilitario</h3>
                        <h4>$ 45,634</h4>
                    </div>
                </div>
            </div>
        </section>

        <hr>

        <section>
            <h2 class="m-b-lg"><span class="m-r"><img src="/img/icon-noticias.png"></span>Bajo Kilometraje</h2>

            <div class="panel featured" style="display: flex;">
                <div clas="row">
                    <div class="col-md-4">
                        <img class="img-responsive" src="/img/mock-car.jpg">

                        <h3>2006 Saab prueba 2 Utilitario</h3>
                        <h4>$ 45,634</h4>
                    </div>
                    <div class="col-md-4">
                        <img class="img-responsive" src="/img/mock-car.jpg">

                        <h3>2006 Saab prueba 2 Utilitario</h3>
                        <h4>$ 45,634</h4>
                    </div>
                    <div class="col-md-4">
                        <img class="img-responsive" src="/img/mock-car.jpg">

                        <h3>2006 Saab prueba 2 Utilitario</h3>
                        <h4>$ 45,634</h4>
                    </div>
                </div>
            </div>
        </section>

        <hr>

        <section>
            <h2 class="m-b-lg"><span class="m-r"><img src="/img/icon-noticias.png"></span>Precio más bajo</h2>

            <div class="panel featured" style="display: flex;">
                <div clas="row">
                    <div class="col-md-4">
                        <img class="img-responsive" src="/img/mock-car.jpg">

                        <h3>2006 Saab prueba 2 Utilitario</h3>
                        <h4>$ 45,634</h4>
                    </div>
                    <div class="col-md-4">
                        <img class="img-responsive" src="/img/mock-car.jpg">

                        <h3>2006 Saab prueba 2 Utilitario</h3>
                        <h4>$ 45,634</h4>
                    </div>
                    <div class="col-md-4">
                        <img class="img-responsive" src="/img/mock-car.jpg">

                        <h3>2006 Saab prueba 2 Utilitario</h3>
                        <h4>$ 45,634</h4>
                    </div>
                </div>
            </div>
        </section>

        <hr>

        <section class="noticias">
            <div class="well">
                <h2 class="m-b-lg"><span class="m-r"><img src="/img/icon-noticias.png"></span>Últimas noticias</h2>

                <div style="display: flex;">
                    <div clas="row">
                        <div class="col-md-4">
                            <img class="img-responsive" src="/img/mock-car.jpg">

                            <h3>2006 Saab prueba 2 Utilitario</h3>
                        </div>
                        <div class="col-md-4">
                            <img class="img-responsive" src="/img/mock-car.jpg">

                            <h3>2006 Saab prueba 2 Utilitario</h3>
                        </div>
                        <div class="col-md-4">
                            <img class="img-responsive" src="/img/mock-car.jpg">

                            <h3>2006 Saab prueba 2 Utilitario</h3>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>

    @include('front.layouts.footer')

@stop

@section('inline_styles')
    <style>
        h2 {
            font-family: 'Open Sans', sans-serif;
            font-weight: bold;
            color: #d93749;
            letter-spacing: 0;
            text-align: center;
            text-transform: uppercase;
        }

        .choose .box {
            padding: 30px 90px
        }

        .choose .box a img {
            display: inline-block;
            width: 100%;
        }

        .concesionarios-destacados .well, .noticias .well {
            -webkit-border-radius: 0;
            -moz-border-radius: 0;
            border-radius: 0;
            border: 0;
        }

        .panel.featured h3 {
            font-size: 20px;
            line-height: 1em;
            margin: 8px 0px;
        }

        .panel.featured h4 {
            font-size: 18px;
            line-height: 1em;
            margin-top: 0;
        }
    </style>
@append