<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="csrf-param" content="_token">

    <title> @yield('page_title') | Comunidad Transportista</title>

    <!-- Latest compiled and minified CSS -->
    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">--}}

    <!-- Optional theme -->
    {{--<link rel="stylesheet" href="/css/style.css">--}}

    <link rel="stylesheet" href="{{ elixir('css/base.css') }}">
    <link rel="stylesheet" href="{{ elixir('css/app.css') }}">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    @if (!app()->isLocal())
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

//        ga('create', 'UA-86350366-1', 'auto');
        ga('send', 'pageview');

    </script>
    @endif
    
    @yield('inline_styles')
</head>
<body>

@if (is_admin())
    <nav class="admin-bar">
        <ul class="nav navbar-nav"><li><a href="#">Dashboard</a></li></ul>
        <ul class="nav navbar-nav"><li><a href="{!! route('admin.activities.index') !!}">Actividades</a></li></ul>
        <ul class="nav navbar-nav"><li><a href="{!! route('admin.users.index') !!}">Usuários</a></li></ul>
    </nav>
@endif

<div class="container-fluid account-layout">
    <div class="row">
        @yield('content')
    </div>
</div>


<script  type="text/javascript" src="{{ elixir('js/bottom.js') }}"></script>

{{--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/retina.js/1.3.0/retina.min.js"></script>--}}

@yield('inline_scripts')
</body>
</html>
