<!-- Static navbar -->
<nav class="navbar navbar-inverse navbar-default" style="margin: 0; border-radius: 0; border: 0;">
    <div class="container-fluid" style="padding: 0;">
        <div class="navbar-header">
            <a class="navbar-brand hidden-xs" href="{!! route('root') !!}" style="height: auto; padding: 0;">
                <img src="/img/logo.png" width="333" height="127">
            </a>
            <a class="navbar-brand-mobile visible-xs" href="{!! route('root') !!}">
                <img src="/img/logo.png" height="30">
            </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse" style="margin-top: 50px;">
            <div class="redes-sociales">
                <a target="blank" href="http://www.facebook.com/comunidadtransportista" class="icon-fb"></a>
                <a target="blank" href="http://instagram.com/comunidadtransportista" class="icon-inst"></a>
                <a target="blank" href="http://twitter.com/c_transportista" class="icon-tw"></a>
            </div>
            <ul class="nav navbar-nav navbar-right">
                @if (current_user())
                    <li class="m-l">
                        <a class="bordered green-3" href="/account">Mi cuenta</a>
                    </li>
                    <li class="m-l">
                        <a class="bordered green-3" href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Salir</a>

                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                @else
                    <li class="m-l special"><a class="bordered green-3" href="/login">Acceder</a></li>
                    <li class="m-l special"><a class="bordered pink" href="/register">Registrate</a></li>
                @endif
            </ul>
        </div>
        <!--/.nav-collapse -->
    </div>
</nav>
<nav class="navbar primary-menu navbar-inverse navbar-default" style="border-radius: 0; margin: 0; border: 0; background-color: #2d2a28;">
    <div class="container-fluid">
        <ul class="nav navbar-nav">
            <li><a class="camiones" href="{!! route('front.camiones.index') !!}">Camiones</a></li>
            <li><a class="buses" href="{!! route('front.buses.index') !!}">Buses</a></li>
            <li><a class="utilitarios" href="{!! route('front.utilitarios.index') !!}">Utilitarios</a></li>
            <li><a href="{!! route('front.noticias.index') !!}">Noticias</a></li>
            <li><a href="{!! route('root') !!}">Nosotros</a></li>
            <li><a href="{!! route('front.contacto.index') !!}">Contacto</a></li>
        </ul>
    </div>
</nav>