@extends('front.layouts.page')

@section('page_title') Test @stop

@section('content')
    @include('front.layouts.nav')

    <nav class="subheader utilitarios">
        <h2>Comunidad Utilitarios</h2>
        <span><img src="/img/utilitario.png"></span>
    </nav>

    <div class="container">
        <div class="col-md-10 col-md-offset-1">
            <section class="details">
                <span class="price pull-right">$300.000</span>

                <h2>IVECO, Euro Tech 190E31 Año 1964</h2>

                <h3>Buenos Aires - GBA Norte</h3>

                <hr>

                <div class="media">
                    <div class="media-left col-md-6 m-r">
                        <a href="#">
                            <img class="media-object img-responsive" src="/img/mock-car.jpg" alt="">
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Especificaciones</h4>

                        <div class="attribute"><label>Aplicación de Uso:</label> Mudanza</div>
                        <div class="attribute"><label>Tipo:</label> Chasis Corto</div>
                        <div class="attribute"><label>Tracción:</label> 6x2</div>
                        <div class="attribute"><label>Capacidad de carga útil:</label> 7 a 10,9 Ton</div>
                        <div class="attribute"><label>Potencia:</label> 100 - 199</div>
                        <div class="attribute"><label>Cilindrada:</label> 2,0 a 3,9</div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    @include('front.layouts.footer')
@stop

@section('inline_styles')
    <style>
        section.details {
            padding: 30px;
            min-height: 500px;
        }

        section.details h2 {
            color: #43bb73;
            text-transform: uppercase;
            font-weight: bold;
            font-family: 'Open Sans', sans-serif;
            font-size: 30px;
            margin: 0;
            line-height: 1em;
        }

        section.details h3 {

            font-size: 18px;
            margin: 3px 0;
            line-height: 1em;
        }

        section.details span.price {
            font-size: 40px;
            font-weight: bold;
            color: black;
            line-height: 1em;
        }

        section.details .media .media-body h4 {
            font-family: 'Open Sans', sans-serif;
            font-weight: bold;
            color: #d93749;
            letter-spacing: 0;
            text-transform: uppercase;
            margin-bottom: 15px;
        }

        section.details .media .media-body .attribute {
            font-size: 14px;
        }

        section.details .media .media-body label {
            color: #000;
            font-weight: bold;
        }
    </style>
@endsection

@section('inline_scripts')
    <script></script>
@append