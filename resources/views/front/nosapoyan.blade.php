@extends('front.layouts.page')

@section('page_title') Nos Apoyan @stop

@section('content')
    @include('front.layouts.nav')

    <section class="cover" style="background-image: url('/img/nosapoyan-bg.jpg');">
        <h1>Nos Apoyan</h1>
    </section>

    <section class="nosapoyan">
        <div class="container-fluid">
            <div class="container">

                <div class="partners row">
                    @foreach(['candle', 'organicspa', 'madeleine', 'grill', 'patagonia', 'directv', 'fila', 'zurich', 'oracle', 'laroche', 'google', 'macro', 'hsbc', 'ups', 'microsoft', 'despegar', 'cresta', 'cordial', 'johnson', 'bovis', 'united', 'mercado'] as $k => $v)
                        <div class="partner col-sm-3">
                            <img class="" src="/img/logos/{!! $v !!}.png">
                        </div>

                    @endforeach
                </div>
            </div>
        </div>
    </section>

    @include('front.layouts.footer')

@stop

@section('inline_styles')
    <style>
        section.cover {
            min-height: 480px;
            background-position: 50% 50%;
        }
        section.cover > h1 {

        }
    </style>
@append