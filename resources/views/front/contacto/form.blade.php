<div class="ibox">
    {!! Form::open(['route' => ['front.contacto.store'], 'method' => 'post']) !!}
    <div class="ibox-title">
        <h3>Formulario de Contacto</h3>
    </div>
    <div class="ibox-content">
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group  col-lg-12">
                {!! Form::label('name', 'Nombre') !!}
                {!! Form::input('text', 'name', null, ['class'=>'form-control', 'placeholder'=>'Nombre...']) !!}
                </div>
                <div class="form-group  col-lg-12">
                {!! Form::label('email', 'Email') !!}
                {!! Form::input('text', 'email', null, ['class'=>'form-control', 'placeholder'=>'Email...']) !!}
                </div>
                <div class="form-group  col-lg-12">
                {!! Form::label('Observaciones', 'Mensaje') !!}
                {!! Form::textarea('Observaciones', null, ['class'=>'form-control', 'placeholder'=>'Enter...', 'id' => '', 'onFocus' => ""]) !!}
                </div>
            </div>
        </div>
        {!! Form::submit('Dejar Mensaje') !!}
    </div>
</div>

    {!! Form::close() !!}