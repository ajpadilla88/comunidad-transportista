@extends('front.layouts.page')

@section('page_title') Noticias @stop

@section('content')
    @include('front.layouts.nav')

    <nav class="subheader default">
        <h2>Cantactos</h2>
    </nav>

    <div class="container">

        @if(Session::has('message_success'))
            <div class="alert alert-success">
                <span class="glyphicon glyphicon-ok"></span>
                @foreach($errors as $error  )
                <em> {{ $error }}</em>
                @endforeach
            </div>
        @endif

        @include('front.contacto.form')

    </div>

    @include('front.layouts.footer')
@stop

@section('inline_styles')
    <style>
        .subheader {
            margin-bottom: 50px;
        }

        .container hr {
            border-top: 1px solid #CCC;
            margin: 30px 0;
        }

        article .media .media-left { padding-left: 0;}

        article .media .media-object {
            padding: 20px;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
            border: 1px solid #ccc;
        }

        article .media .media-body h4 {
            font-family: 'Open Sans', sans-serif;
            font-weight: lighter;
            font-size: 20px;
            color: #d93749;
            letter-spacing: 0;
            text-transform: uppercase;
            margin-bottom: 5px;
        }

        article .media .media-body h2 {
            font-family: 'Open Sans', sans-serif;
            /*font-weight: bold;*/
            margin-top: 0;
            color: #d93749;
            font-size: 30px;
            margin-bottom: 15px;
        }

        article .media .media-body .attribute {
            font-size: 14px;
        }

        article .media .media-body label {
            color: #000;
            font-weight: bold;
        }

        article .media .media-body a.btn {
            background-color: #d93649;
            -webkit-border-radius: 0;
            -moz-border-radius: 0;
            border-radius: 0;
            border: #d93649;
        }

    </style>
@endsection

@section('inline_scripts')
    <script></script>
@append