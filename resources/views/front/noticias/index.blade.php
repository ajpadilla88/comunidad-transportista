@extends('front.layouts.page')

@section('page_title') Noticias @stop

@section('content')
    @include('front.layouts.nav')

    <nav class="subheader default">
        <h2>Noticias</h2>
    </nav>

    <div class="container">
        @for($i = 0; $i < 10; $i++)
            <article>
                <div class="media">
                    <div class="media-left col-md-6 m-r">
                        <a href="#">
                            <img class="media-object img-responsive" src="/img/mock-noticia.jpg" alt="">
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">JUN 02</h4>
                        <h2 class="media-heading">Conoce el ranking de patentamientos de Mayo</h2>

                        <p>
                            En este mes, se logró pasar las 2000 unidades nuevamente. Se patentaron un total de 2236 vehículos en el mercado de vehículos comerciales pesados. Desde enero que no se superaba ese número. Para darnos una idea, el mismo mes del año pasado se registraron 1431, un 56% menos. La marca que lideró el mercado fue Mercedes Benz. El segundo, Iveco y seguido por Ford. Volkswagen quedo cuarto, perdiendo el tercer puesto que mantiene desde el inicio de a...
                        </p>

                        <a href="#" class="btn btn-primary">Leer +</a>
                    </div>
                </div>
            </article>
            <hr>
        @endfor
    </div>

    @include('front.layouts.footer')
@stop

@section('inline_styles')
    <style>
        .subheader {
            margin-bottom: 50px;
        }

        .container hr {
            border-top: 1px solid #CCC;
            margin: 30px 0;
        }

        article .media .media-left { padding-left: 0;}

        article .media .media-object {
            padding: 20px;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
            border: 1px solid #ccc;
        }

        article .media .media-body h4 {
            font-family: 'Open Sans', sans-serif;
            font-weight: lighter;
            font-size: 20px;
            color: #d93749;
            letter-spacing: 0;
            text-transform: uppercase;
            margin-bottom: 5px;
        }

        article .media .media-body h2 {
            font-family: 'Open Sans', sans-serif;
            /*font-weight: bold;*/
            margin-top: 0;
            color: #d93749;
            font-size: 30px;
            margin-bottom: 15px;
        }

        article .media .media-body .attribute {
            font-size: 14px;
        }

        article .media .media-body label {
            color: #000;
            font-weight: bold;
        }

        article .media .media-body a.btn {
            background-color: #d93649;
            -webkit-border-radius: 0;
            -moz-border-radius: 0;
            border-radius: 0;
            border: #d93649;
        }

    </style>
@endsection

@section('inline_scripts')
    <script></script>
@append