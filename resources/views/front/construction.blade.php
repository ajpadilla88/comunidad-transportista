@extends('front.layouts.page')

@section('page_title') Home @stop

@section('content')
    @include('front.layouts.nav')

    <section class="construction">
        <div class="container-fluid">
            <div class="container text-center">
                <img src="/img/construccion.svg" width="500">
                <h1>Estamos en proceso de construcción</h1>
                <p>
                    Esta página del sitio se encuentra en desarrollo.<br>
                    ¡Gracias por tu visita! Visitanos de nuevo pronto.
                </p>
            </div>
        </div>
    </section>

    @include('front.layouts.footer')

    <style>
        body { background-color: white;}
    </style>
@stop