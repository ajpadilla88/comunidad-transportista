@extends('front.layouts.page')

@section('page_title') Aliados @stop

@section('content')
    @include('front.layouts.nav')

    <section class="cover" style="background-image: url('/img/arboles.jpg');">
        <h1>Organizaciones Aliadas</h1>
    </section>

    <section class="nosapoyan">
        <div class="container-fluid">
            <div class="container">

                <div class="partners row">
                    @foreach(['escuela501','escuela502', 'bolivar', 'rotary', 'unsam', 'colegio-salvador', 'unla', 'reserva pilar', 'casa nueva', 'eno', 'miguel', 'dda', 'conciencia', 'potencialidades', 'recs', 'sol storni', 'hopening', 'cascos verdes',  'semana', 'best buddies', 'dinad', 'tesa', 'ieladeinu', 'utn', 'san andres', 'grupo arboles', 'borda', 'le dor', 'tuverde.com', 'almagro', 'asac', 'newlands', 'inta', 'vivero eco'] as $k => $v)
                        <div class="partner col-sm-3">
                            <img class="" src="/img/logos/aliados/{!! $v !!}.png">
                        </div>

                    @endforeach
                </div>
            </div>
        </div>
    </section>

    @include('front.layouts.footer')

@stop