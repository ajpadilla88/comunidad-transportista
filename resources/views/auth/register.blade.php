@extends('front.layouts.page')

@section('page_title') Registrate @stop

@section('content')
    @include('front.layouts.nav')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="login-box">
                    <div class="row">
                        <nav class="subheader default">
                            <h2>Registrate</h2>
                        </nav>
                        <div class="col-md-8 left-side">
                            <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                                {{ csrf_field() }}

                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                <div class="groupped">
                                    <div class="pull-left form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                        <label for="first_name" class="col-md-4 control-label">Nombre</label>
                                        <input id="first_name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" required autofocus>
                                    </div>

                                    <div class="pull-right form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                        <label for="last_name" class="col-md-4 control-label">Apellido</label>
                                        <input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" required autofocus>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="col-md-4 control-label">Email</label>
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                                </div>

                                <div class="groupped">
                                    <div class="pull-left form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label for="password" class="col-md-4 control-label">Contraseña</label>
                                        <input id="password" type="password" class="form-control" name="password" required>
                                    </div>

                                    <div class="pull-right form-group">
                                        <label for="password-confirm" class="col-md-4 control-label">Confirma
                                            Contraseña</label>
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                    </div>
                                </div>

                                <div class="groupped">
                                    <div class="pull-left form-group{{ $errors->has('state_id') ? ' has-error' : '' }}">
                                        <label for="state_id" class="col-md-4 control-label">Provincia</label>
                                        {!! Form::select('state_id', $states->pluck('name', 'id'), null,  ['class'=>'form-control', 'required', 'placeholder'=>'Elija su Provincia', 'style' => 'width: 100%; height: 44px;']) !!}
                                        {{--<select id="state_id" class="form-control" name="state_id" required></select>--}}
                                    </div>

                                    <div class="pull-right form-group">
                                        <label for="postal_code" class="col-md-4 control-label">Código Postal</label>
                                        <input id="postal_code" type="text" class="form-control" name="postal_code" required>
                                    </div>
                                </div>

                                <div class="form-group m-t">
                                    <button type="submit" class="btn btn-primary btn-lg">Registrarme</button>
                                </div>

                                <hr>

                                <div class="form-group">
                                    <a class="m-r" href="{{ url('/login') }}">Ya tengo cuenta</a>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-4 right-side social">
                            <a class="btn btn-default btn-block" href="#"><i class="fa fa-fw fa-google"></i> Ingresar con Google</a>
                            <a class="btn btn-default btn-block" href="#"><i class="fa fa-fw fa-facebook"></i> Ingresar con Facebook</a>
                            {{--<a class="btn btn-default btn-block" href="#">Ingresar con Twitter</a>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('front.layouts.footer')
@endsection
