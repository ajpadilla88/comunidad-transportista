@extends('front.layouts.page')

@section('page_title') Ingresa @stop

@section('content')
    @include('front.layouts.nav')

    <div class="container" style="min-height: 600px;">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="login-box">
                    <div class="row">
                        <nav class="subheader default">
                            <h2>Iniciar Sesión</h2>
                        </nav>
                        <div class="col-md-8 left-side">

                            <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                                {{ csrf_field() }}

                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                <div class="groupped">
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

                                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
                                    </div>

                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                                        <input id="password" type="password" class="form-control" name="password" required placeholder="Contraseña">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="remember"> Recordarme
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group m-t-lg">
                                    <button type="submit" class="btn btn-primary btn-lg">Ingresar</button>
                                </div>

                                <hr>

                                <div class="form-group">
                                    <a class="m-r" href="{{ route('password.request') }}">Olvidé mi contraseña</a>
                                    |
                                    <a class="m-l" href="{{ route('register') }}">Registrarme</a>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-4 right-side social">
                            <a class="btn btn-default btn-block" href="#"><i class="fa fa-fw fa-google"></i> Ingresar con Google</a>
                            <a class="btn btn-default btn-block" href="#"><i class="fa fa-fw fa-facebook"></i> Ingresar con Facebook</a>
                            {{--<a class="btn btn-default btn-block" href="#"><i class="fa fa-fw fa-twitter"></i> Ingresar con Twitter</a>--}}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('front.layouts.footer')
@endsection

@section('inline_scripts')
    <script>
        jQuery(function($) {
            $('#email').focus();
        });
    </script>
@append